# linear_systems.py
"""Volume 1: Linear Systems.
<Tyler Humpherys>
<Math 345>
<9/28/21>
"""
import numpy as np
import scipy.linalg as la
import time
from matplotlib import pyplot as plt
from scipy import sparse
from scipy.sparse import linalg as spla


# Problem 1
def ref(A):
    """Reduce the square matrix A to REF. You may assume that A is invertible
    and that a 0 will never appear on the main diagonal. Avoid operating on
    entries that you know will be 0 before and after a row operation.

    Parameters:
        A ((n,n) ndarray): The square invertible matrix to be reduced.

    Returns:
        ((n,n) ndarray): The REF of A.
    """
    A = A.astype(float)
    
    def cols(A):
        """Create zeros under the leading entry by adding multiples of 
        other rows"""
        for j in range(1, len(A)):
            A[j, 0:] -= (A[j, 0] / A[0,0]) * A[0]
        return A
    
    #Iterate through the columns and create zeros under the leading entry by 
    #adding multiples of other rows
    for i in range(0, len(A)):
        A[i:, i:] = cols(A[i:, i:])
    
    return A

# Problem 2
def lu(A):
    """Compute the LU decomposition of the square matrix A. You may
    assume that the decomposition exists and requires no row swaps.

    Parameters:
        A ((n,n) ndarray): The matrix to decompose.

    Returns:
        L ((n,n) ndarray): The lower-triangular part of the decomposition.
        U ((n,n) ndarray): The upper-triangular part of the decomposition.
    """
    #Store the dimensions of A
    m, n = A.shape[0], A.shape[1]
    
    #Make U a copy of A and set L equal to the identity
    U = np.copy(A).astype(float)
    L = np.identity(m)
    
    #Compute L and U
    for j in range(n - 1):
        for i in range(j + 1, m):
            L[i, j] = U[i, j]/ U[j, j]
            U[i, j:] = U[i, j:] - L[i, j] * U[j, j:]
    return L, U

# Problem 3
def solve(A, b):
    """Use the LU decomposition and back substitution to solve the linear
    system Ax = b. You may again assume that no row swaps are required.

    Parameters:
        A ((n,n) ndarray)
        b ((n,) ndarray)

    Returns:
        x ((m,) ndarray): The solution to the linear system.
    """
    #Get the LU decomposition of A
    L, U = lu(A)
    
    #Use forward substitution to solve for y.
    y = np.zeros(len(b))
    for i in range(len(b)):
        y[i] = (b[i] - np.dot(y, L[i,:])) / L[i,i]
    
    #Now use back substitution to solve for x to solve the linear system Ax=b.    
    x = np.zeros(len(y))
    for j in range(len(y) - 1, -1, -1):
        x[j] = (y[j] - np.dot(x, U[j,:])) / U[j,j]        
    return x

# Problem 4
def prob4():
    """Time different scipy.linalg functions for solving square linear systems.

    For various values of n, generate a random nxn matrix A and a random
    n-vector b using np.random.random(). Time how long it takes to solve the
    system Ax = b with each of the following approaches:

        1. Invert A with la.inv() and left-multiply the inverse to b.
        2. Use la.solve().
        3. Use la.lu_factor() and la.lu_solve() to solve the system with the
            LU decomposition.
        4. Use la.lu_factor() and la.lu_solve(), but only time la.lu_solve()
            (not the time it takes to do the factorization).

    Plot the system size n versus the execution times. Use log scales if
    needed.
    """
    #Create lists to store x and y axis data
    n_vals = [2**n for n in range(1,9)]
    inv_times = []
    solve1_times = []
    factor_solve_times = []
    solve2_times = []
    
    for n in n_vals:
        #Create random matrices 
        A = np.random.random((n,n))
        b = np.random.random(n)
        
        #Time la.inv() to invert A and left-multiply the inverse to b
        start = time.time()
        la.inv(A)@b
        comp_time = time.time() - start
        inv_times.append(comp_time)
        
        #Time la.solve()
        start = time.time()
        la.solve(A, b)
        comp_time = time.time() - start
        solve1_times.append(comp_time)
        
        #Time la.lu_factor() and la.lu_solve()
        start = time.time()
        L, P = la.lu_factor(A)
        la.lu_solve((L, P), b)
        comp_time = time.time() - start
        factor_solve_times.append(comp_time)
        
        #Use la.lu_factor() and la.lu_solve() but only time la.lu_solve()
        L, P = la.lu_factor(A)
        start = time.time()
        la.lu_solve((L, P), b)
        comp_time = time.time() - start
        solve2_times.append(comp_time)
    
    #Plot computation times on a linear scale
    plt.subplot(121)
    plt.plot(n_vals, inv_times, '.-', color='tab:blue', label="Using inv()")
    plt.plot(n_vals, solve1_times, '.-', color='tab:orange', label="Using solve()")
    plt.plot(n_vals, factor_solve_times, '.-', color='tab:red', label="Using lu_factor() & lu_solve()")
    plt.plot(n_vals, solve2_times, '.-', color='tab:green', label="Only time lu_solve((L, P),b)")
    plt.xlabel("n")
    plt.legend(loc='upper left')
    plt.ylabel("Seconds")
    plt.title("Linear scale")
    
    #Plot computation times on a log-log scale
    plt.subplot(122)
    plt.loglog(n_vals, inv_times, '.-', color='tab:blue')
    plt.loglog(n_vals, solve1_times, '.-', color='tab:orange')
    plt.loglog(n_vals, factor_solve_times, '.-', color='tab:red')
    plt.loglog(n_vals, solve2_times, '.-', color='tab:green')
    plt.title("Log-log scale")
    plt.legend(loc='upper left')
    plt.xlabel("n")
    plt.figure(figsize=(50,20))
    plt.show()

# Problem 5
def prob5(n):
    """Let I be the n × n identity matrix, and define
                    [B I        ]        [-4  1            ]
                    [I B I      ]        [ 1 -4  1         ]
                A = [  I . .    ]    B = [    1  .  .      ],
                    [      . . I]        [          .  .  1]
                    [        I B]        [             1 -4]
    where A is (n**2,n**2) and each block B is (n,n).
    Construct and returns A as a sparse matrix.

    Parameters:
        n (int): Dimensions of the sparse matrix B.

    Returns:
        A ((n**2,n**2) SciPy sparse matrix)
    """
    #Create diagonals and offset values
    diagonals = [1,-4,1]
    offsets = [-1,0,1]

    #Create sparse matrix
    B = sparse.diags(diagonals, offsets, (n,n))
    A = sparse.block_diag([B]*n)
    
    A.setdiag(1, n)
    A.setdiag(1, -n)
    
    return A

# Problem 6
def prob6():
    """Time regular and sparse linear system solvers.

    For various values of n, generate the (n**2,n**2) matrix A described of
    prob5() and vector b of length n**2. Time how long it takes to solve the
    system Ax = b with each of the following approaches:

        1. Convert A to CSR format and use scipy.sparse.linalg.spsolve()
        2. Convert A to a NumPy array and use scipy.linalg.solve().

    In each experiment, only time how long it takes to solve the system (not
    how long it takes to convert A to the appropriate format). Plot the system
    size n**2 versus the execution times. As always, use log scales where
    appropriate and use a legend to label each line.
    """
    #Create lists to store x and y axis data
    n_vals = [n**2 for n in range(2,10)]
    spla_spsolve_times = []
    la_solve_times = []
    
    for n in n_vals:
        #Create random matrix and vector
        A = prob5(n)
        b = np.random.random(n**2)
        
        #Time spla.spsolve()
        A = A.tocsr() 
        start = time.time()
        spla.spsolve(A, b)
        comp_time = time.time() - start
        spla_spsolve_times.append(comp_time)
        
        #Time la.solve()
        A_array = A.toarray()
        start = time.time()
        la.solve(A_array, b)
        comp_time = time.time() - start
        la_solve_times.append(comp_time)
    
    #Plot computation times on a linear scale
    plt.subplot(121)
    plt.plot(n_vals, spla_spsolve_times, '.-', color='tab:blue', label="Using spla.spsolve()")
    plt.plot(n_vals, la_solve_times, '.-', color='tab:orange', label="Using la.solve()")
    plt.xlabel("n^2")
    plt.legend(loc='upper left')
    plt.ylabel("Seconds")
    plt.title("Linear scale")
    
    #Plot computation times on a log-log scale
    plt.subplot(122)
    plt.loglog(n_vals, spla_spsolve_times, '.-', color='tab:blue')
    plt.loglog(n_vals, la_solve_times, '.-', color='tab:orange')
    plt.title("Log-log scale")
    plt.legend(loc='upper left')
    plt.xlabel("n^2")
    plt.figure(figsize=(50,20))
    plt.show()


