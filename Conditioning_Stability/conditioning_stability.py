# condition_stability.py
"""Volume 1: Conditioning and Stability.
<Tyler Humpherys>
<Math 347>
<2/1/22>
"""

import numpy as np
import sympy as sy
import scipy.linalg as la
from matplotlib import pyplot as plt

# Problem 1
def matrix_cond(A):
    """Calculate the condition number of A with respect to the 2-norm."""
    #Get the singular values of A
    sing_vals = la.svdvals(A)
    
    #If the smallest singular values is zero return infinity
    if sing_vals[-1] == 0:
        return np.inf
    else:
        return sing_vals[0]/sing_vals[-1]


# Problem 2
def prob2():
    """Randomly perturb the coefficients of the Wilkinson polynomial by
    replacing each coefficient c_i with c_i*r_i, where r_i is drawn from a
    normal distribution centered at 1 with standard deviation 1e-10.
    Plot the roots of 100 such experiments in a single figure, along with the
    roots of the unperturbed polynomial w(x).

    Returns:
        (float) The average absolute condition number.
        (float) The average relative condition number.
    """
    #Initialize lists to store 
    abs_condition, rel_condition = [], []
    
    for _ in range(100):
        #The roots of w are 1, 2, ..., 20.
        w_roots = np.arange(1, 21)
    
        #Get the exact Wilkinson polynomial coefficients using SymPy.
        x, i = sy.symbols('x i')
        w = sy.poly_from_expr(sy.product(x-i, (i, 1, 20)))[0]
        w_coeffs = np.array(w.all_coeffs())
    
        #Perturb one of the coefficients very slightly.
        h = np.random.normal(1, 10**-10, 21)
        new_coeffs = w_coeffs * h
        
        #Use NumPy to compute the roots of the perturbed polynomial.
        new_roots = np.roots(np.poly1d(new_coeffs))
        
        #Sort the roots to ensure that they are in the same order.
        w_roots = np.sort(w_roots)
        new_roots = np.sort(new_roots)
        
        #Plot new perterbed roots
        plt.scatter(np.real(new_roots), np.imag(new_roots), s=1, c='black', marker=',', alpha=0.5)
        
        #Estimate the absolute condition number in the infinity norm.
        k = la.norm(new_roots - w_roots, np.inf) / la.norm(h, np.inf)
        abs_condition.append(k)
        
        #Estimate the relative condition number in the infinity norm.
        k_rel = k * la.norm(w_coeffs, np.inf) / la.norm(w_roots, np.inf)
        rel_condition.append(k_rel)
    
    #Plot the perturbed roots
    plt.scatter(np.real(w_roots), np.imag(w_roots))
    plt.xlabel("Real Axis")
    plt.ylabel("Imaginary Axis")
    plt.title("Roots of Wilkinson Polynomial")
    plt.show()
    
    return np.mean(abs_condition), np.mean(rel_condition)
    

# Helper function
def reorder_eigvals(orig_eigvals, pert_eigvals):
    """Reorder the perturbed eigenvalues to be as close to the original eigenvalues as possible.
    
    Parameters:
        orig_eigvals ((n,) ndarray) - The eigenvalues of the unperturbed matrix A
        pert_eigvals ((n,) ndarray) - The eigenvalues of the perturbed matrix A+H
        
    Returns:
        ((n,) ndarray) - the reordered eigenvalues of the perturbed matrix
    """
    n = len(pert_eigvals)
    sort_order = np.zeros(n).astype(int)
    dists = np.abs(orig_eigvals - pert_eigvals.reshape(-1,1))
    for _ in range(n):
        index = np.unravel_index(np.argmin(dists), dists.shape)
        sort_order[index[0]] = index[1]
        dists[index[0],:] = np.inf
        dists[:,index[1]] = np.inf
    return pert_eigvals[sort_order]

# Problem 3
def eig_cond(A):
    """Approximate the condition numbers of the eigenvalue problem at A.

    Parameters:
        A ((n,n) ndarray): A square matrix.

    Returns:
        (float) The absolute condition number of the eigenvalue problem at A.
        (float) The relative condition number of the eigenvalue problem at A.
    """
    #Create the perturbation matrix H
    reals = np.random.normal(0, 1e-10, A.shape)
    imags = np.random.normal(0, 1e-10, A.shape)
    H = reals + 1j*imags
    
    #Compute the eigenvalues of A and A+H
    A_eigs, AH_eigs = la.eigvals(A), la.eigvals(A + H)
    
    #Reorder the perterbed eigenvalues for A+H
    AH_reorder_eigs = reorder_eigvals(A_eigs, AH_eigs)
    
    #Compute the absolute and relative condition numbers
    abs_condition = la.norm(A_eigs - AH_reorder_eigs, 2)/la.norm(H, 2)
    rel_condition = la.norm(A, 2)/la.norm(A_eigs, 2) * abs_condition
    
    return abs_condition, rel_condition


# Problem 4
def prob4(domain=[-100, 100, -100, 100], res=50):
    """Create a grid [x_min, x_max] x [y_min, y_max] with the given resolution. For each
    entry (x,y) in the grid, find the relative condition number of the
    eigenvalue problem, using the matrix   [[1, x], [y, 1]]  as the input.
    Use plt.pcolormesh() to plot the condition number over the entire grid.

    Parameters:
        domain ([x_min, x_max, y_min, y_max]):
        res (int): number of points along each edge of the grid.
    """
    #Initialize the matrix A and linspaces
    A = np.empty((res, res))
    x, y = np.linspace(domain[0], domain[1], res), np.linspace(domain[2], domain[3], res)
    
    for i in range(res):
        for j in range(res):
            #Compute the entries of A
            rel_condition = eig_cond(np.array([[1, x[i]], [y[j], 1]]))[1]
            A[i][j] = rel_condition
    
    #Plot
    plt.pcolormesh(x, y, A, cmap='gray_r')
    plt.colorbar()
    plt.xlabel("x values")
    plt.ylabel("y values")
    plt.title("Relative Condition Numbers")
    plt.show()


# Problem 5
def prob5(n):
    """Approximate the data from "stability_data.npy" on the interval [0,1]
    with a least squares polynomial of degree n. Solve the least squares
    problem using the normal equation and the QR decomposition, then compare
    the two solutions by plotting them together with the data. Return
    the mean squared error of both solutions, ||Ax-b||_2.

    Parameters:
        n (int): The degree of the polynomial to be used in the approximation.

    Returns:
        (float): The forward error using the normal equations.
        (float): The forward error using the QR decomposition.
    """
    #Get the data
    xk, yk = np.load("stability_data.npy").T
    A = np.vander(xk, n+1)
    
    #Compute the least squares solution solution1 using la.inv()
    solution1 = la.inv(A.T@A)@A.T@yk
    
    #Compute the least squares solution using la.qr()
    Q, R = la.qr(A, mode='economic')
    solution2 = la.solve_triangular(R, Q.T@yk)
    
    #Plot
    plt.scatter(xk, yk)
    plt.plot(xk, np.polyval(solution1, xk), c='r', label='Inverse Method')
    plt.plot(xk, np.polyval(solution2, xk), c='orange', label='QR Method')
    plt.legend(loc='upper right')
    plt.xlabel("X values")
    plt.ylabel("Y values")
    plt.title("Least Squares Solution")
    plt.show()
    
    #Compute the forward errors
    inv_condition, QR_condition = la.norm(A@solution1 - yk, 2), la.norm(A@solution2 - yk, 2)
    return inv_condition, QR_condition
    

# Problem 6
def prob6():
    """For n = 5, 10, ..., 50, compute the integral I(n) using SymPy (the
    true values) and the subfactorial formula (may or may not be correct).
    Plot the relative forward error of the subfactorial formula for each
    value of n. Use a log scale for the y-axis.
    """
    #Initialize n values and symbol x
    n_vals, x, errors_list = [5*i for i in range(1, 11)], sy.Symbol("x"), []
    
    for n in n_vals:
        #Create expression I1 and ealuate the integral
        I1_expression = x**n * sy.exp(x - 1)
        I1_integral = sy.integrate(I1_expression, (x, 0, 1))
        
        #Create expression I2 and ealuate the expression
        I2_result = (-1)**n * (sy.subfactorial(n) - (sy.factorial(n)/np.e))
        
        #Compute the error and append
        errors_list.append(np.abs(I1_integral - I2_result))
    
    #Plot
    plt.plot(n_vals, errors_list)
    plt.xlabel("n values")
    plt.yscale("log")
    plt.ylabel("Error")
    plt.title("Error of Evaluating I(n)")
    plt.show()
    

