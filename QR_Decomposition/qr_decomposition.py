# qr_decomposition.py
"""Volume 1: The QR Decomposition.
<Tyler Humpherys>
<Math 345>
<10/19/21>
"""
import numpy as np
from scipy import linalg as la

# Problem 1
def qr_gram_schmidt(A):
    """Compute the reduced QR decomposition of A via Modified Gram-Schmidt.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n.

    Returns:
        Q ((m,n) ndarray): An orthonormal matrix.
        R ((n,n) ndarray): An upper triangular matrix.
    """
    #Get the dimensions of A
    m, n = np.shape(A)[0], np.shape(A)[1]
    
    #Make a copy of A
    Q = A.copy()
    
    #Make an nxn array of all zeros
    R = np.zeros((n, n))
    
    #For the columns in Q, normalize the ith column
    for i in range(n):
        R[i, i] = la.norm(Q[:, i])
        Q[:, i] = Q[:, i]/R[i, i]
    
        #Orthogonalize the jth column of Q
        for j in range(i + 1, n):
            R[i, j] = Q[:, j].T@Q[:, i]
            Q[:, j] = Q[:, j] - R[i, j]*Q[:, i]
    
    return Q, R


# Problem 2
def abs_det(A):
    """Use the QR decomposition to efficiently compute the absolute value of
    the determinant of A.

    Parameters:
        A ((n,n) ndarray): A square matrix.

    Returns:
        (float) the absolute value of the determinant of A.
    """
    return abs(np.prod(np.diag(la.qr(A)[1])))


# Problem 3
def solve(A, b):
    """Use the QR decomposition to efficiently solve the system Ax = b.

    Parameters:
        A ((n,n) ndarray): An invertible matrix.
        b ((n, ) ndarray): A vector of length n.

    Returns:
        x ((n, ) ndarray): The solution to the system Ax = b.
    """
    #Get Q and R
    Q, R = qr_gram_schmidt(A)
    
    #Compute y
    y = Q.T@b
    
    #Initialize x array
    x = np.zeros(b.shape)
    n = len(y)
    
    #Use back subsitution to solve Rx = y for x
    for i in range(n-1, -1, -1):
        x[i] = (y[i] - sum([x[j] * R[i][j] for j in range(i + 1, n)])) / R[i][i]
        
    return x


# Problem 4
def qr_householder(A):
    """Compute the full QR decomposition of A via Householder reflections.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n.

    Returns:
        Q ((m,m) ndarray): An orthonormal matrix.
        R ((m,n) ndarray): An upper triangular matrix.
    """
    #Define new sign function
    sign = lambda x: 1 if x >= 0 else -1
    
    #Get the dimensions of A
    m, n = np.shape(A)[0], np.shape(A)[1]
    
    #Make a copy of A
    R = A.copy()
    
    #Make an mxm identity matrix
    Q = np.identity(m)
    
    #Normalize u and apply reflection to R and Q
    for k in range(n):
        u = R[k:, k].copy()
        u[0] = u[0] + sign(u[0])*la.norm(u)
        u = u/la.norm(u)
        R[k:, k:] = R[k:, k:] - 2* np.outer(u, (u.T@R[k:, k:]))
        Q[k:, :] = Q[k:, :] - 2* np.outer(u, (u.T@Q[k:, :]))
    
    return Q.T, R


# Problem 5
def hessenberg(A):
    """Compute the Hessenberg form H of A, along with the orthonormal matrix Q
    such that A = QHQ^T.

    Parameters:
        A ((n,n) ndarray): An invertible matrix.

    Returns:
        H ((n,n) ndarray): The upper Hessenberg form of A.
        Q ((n,n) ndarray): An orthonormal matrix.
    """
    #Get the dimensions of A
    m, n = np.shape(A)[0], np.shape(A)[1]
    
    #Make a copy of A
    H = A.copy()
    
    #Make an mxm identity matrix
    Q = np.identity(m)
    
    #Normalize u and apply Qk to H, Qk^T to H and Qk to Q
    for k in range(n - 2):
        u = H[k+1:, k].copy()
        u[0] = u[0] + np.sign(u[0])*la.norm(u)
        u = u/la.norm(u)
        H[k+1:, k:] = H[k+1:, k:] - 2* np.outer(u, (u.T@H[k+1:, k:]))
        H[:, k+1:] = H[:, k+1:] - 2* np.outer((H[:, k+1:]@u), u.T)
        Q[k+1:, :] = Q[k+1:, :] - 2* np.outer(u, (u.T@Q[k+1:, :]))
    
    return H, Q.T

