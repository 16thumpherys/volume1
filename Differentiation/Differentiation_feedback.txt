01/22/22 09:37

Problem 1 (5 points):
Score += 5

Problem 2 (5 points):
Score += 5

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
Score += 10

Problem 5 (5 points):
Score += 5

Problem 6 (5 points):
Put all of the graphs on the same plot
Score += 3

Problem 7 (5 points):
Score += 5

Code Quality (5 points):
Score += 5

Total score: 48/50 = 96.0%

Great job!


Comments:
	Awesome!

-------------------------------------------------------------------------------

01/26/22 20:16

Problem 1 (5 points):
Score += 5

Problem 2 (5 points):
Score += 5

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
Score += 10

Problem 5 (5 points):
Score += 5

Problem 6 (5 points):
Score += 5

Problem 7 (5 points):
Score += 5

Code Quality (5 points):
Score += 5

Total score: 50/50 = 100.0%

Excellent!


Comments:
	Awesome!

-------------------------------------------------------------------------------

