# differentiation.py
"""Volume 1: Differentiation.
<Tyler Humpherys>
<Math 347>
<1/18/22>
"""
import numpy as np
import sympy as sy
from autograd import numpy as anp
from autograd import grad, elementwise_grad
from matplotlib import pyplot as plt
import time

# Problem 1
def prob1():
    """Return the derivative of (sin(x) + 1)^sin(cos(x)) using SymPy."""
    #Create symbols and function
    x = sy.symbols("x")
    f = (sy.sin(x) + 1)**(sy.sin(sy.cos(x)))
    
    #Differentiate
    df = sy.lambdify(x, sy.diff(f, x), "numpy")
    return df

# Problem 2
def fdq1(f, x, h=1e-5):
    """Calculate the first order forward difference quotient of f at x."""
    return (f(x + h) - f(x))/h

def fdq2(f, x, h=1e-5):
    """Calculate the second order forward difference quotient of f at x."""
    return (-3*f(x) + 4*f(x + h) - f(x + 2*h))/(2*h)

def bdq1(f, x, h=1e-5):
    """Calculate the first order backward difference quotient of f at x."""
    return (f(x) - f(x - h))/h

def bdq2(f, x, h=1e-5):
    """Calculate the second order backward difference quotient of f at x."""
    return (3*f(x) - 4*f(x - h) + f(x - 2*h))/(2*h)

def cdq2(f, x, h=1e-5):
    """Calculate the second order centered difference quotient of f at x."""
    return (f(x + h) - f(x - h))/(2*h)

def cdq4(f, x, h=1e-5):
    """Calculate the fourth order centered difference quotient of f at x."""
    return (f(x - 2*h) - 8*f(x - h) + 8*f(x + h) - f(x + 2*h))/(12*h)


# Problem 3
def prob3(x0):
    """Let f(x) = (sin(x) + 1)^(sin(cos(x))). Use prob1() to calculate the
    exact value of f'(x0). Then use fdq1(), fdq2(), bdq1(), bdq2(), cdq1(),
    and cdq2() to approximate f'(x0) for h=10^-8, 10^-7, ..., 10^-1, 1.
    Track the absolute error for each trial, then plot the absolute error
    against h on a log-log scale.

    Parameters:
        x0 (float): The point where the derivative is being approximated.
    """
    #Get derivative and compute f'(x0)
    x = sy.symbols("x")
    fx, df = sy.lambdify(x, (sy.sin(x) + 1)**(sy.sin(sy.cos(x))), "numpy"), prob1()
    value = df(x0)
    
    #Initiate values
    f_one, f_two, b_one, b_two, c_two, c_four = [], [], [], [], [], []
    t1 = np.logspace(-8, 0, 9)
    
    for h in t1:
        #Compute error for each order type
        f_one.append(np.abs(value - fdq1(fx, x0, h)))
        f_two.append(np.abs(value - fdq2(fx, x0, h)))
        b_one.append(np.abs(value - bdq1(fx, x0, h)))
        b_two.append(np.abs(value - bdq2(fx, x0, h)))
        c_two.append(np.abs(value - cdq2(fx, x0, h)))
        c_four.append(np.abs(value - cdq4(fx, x0, h)))
    
    #Plot
    plt.loglog(t1, f_one, '-o', label='Order 1 Forward')
    plt.loglog(t1, f_two, '-o', label='Order 2 Forward')
    plt.loglog(t1, b_one, '-o', label='Order 1 Backward')
    plt.loglog(t1, b_two, '-o', label='Order 2 Backward')
    plt.loglog(t1, c_two, '-o', label='Order 2 Centered')
    plt.loglog(t1, c_four, '-o', label='Order 4 Centered')
    plt.xlabel("h")
    plt.ylabel("Absolute Error")
    plt.title("Error Against h Value")
    plt.legend(loc='upper left', prop={'size': 8})
    plt.show()

# Problem 4
def prob4():
    """The radar stations A and B, separated by the distance 500m, track a
    plane C by recording the angles alpha and beta at one-second intervals.
    Your goal, back at air traffic control, is to determine the speed of the
    plane.

    Successive readings for alpha and beta at integer times t=7,8,...,14
    are stored in the file plane.npy. Each row in the array represents a
    different reading; the columns are the observation time t, the angle
    alpha (in degrees), and the angle beta (also in degrees), in that order.
    The Cartesian coordinates of the plane can be calculated from the angles
    alpha and beta as follows.

    x(alpha, beta) = a tan(beta) / (tan(beta) - tan(alpha))
    y(alpha, beta) = (a tan(beta) tan(alpha)) / (tan(beta) - tan(alpha))

    Load the data, convert alpha and beta to radians, then compute the
    coordinates x(t) and y(t) at each given t. Approximate x'(t) and y'(t)
    using a first order forward difference quotient for t=7, a first order
    backward difference quotient for t=14, and a second order centered
    difference quotient for t=8,9,...,13. Return the values of the speed at
    each t.
    """
    #Load data
    data = np.load('plane.npy')
    
    #Change the degrees to radians for alpha and beta
    data[:, 1] = np.deg2rad(data[:, 1])
    data[:, 2] = np.deg2rad(data[:, 2])
    
    #Initialize values
    a, x_coor, x_prime, y_coor, y_prime, speed = 500, [], [], [], [], []
    
    for t in data:
        #Compute x and y position
        x_coor.append((a * np.tan(t[2]))/(np.tan(t[2]) - np.tan(t[1])))
        y_coor.append((a * np.tan(t[2]) * np.tan(t[1]))/(np.tan(t[2]) - np.tan(t[1])))
        
    #Compute x and y position derivatives for t=7
    x_prime.append(x_coor[1] - x_coor[0])
    y_prime.append(y_coor[1] - y_coor[0])
    
    #Compute x and y position derivatives for t=8,9,...,13
    for i in range(1,7):
        x_prime.append((x_coor[i + 1] - x_coor[i - 1])/2)
        y_prime.append((y_coor[i + 1] - y_coor[i - 1])/2)
    
    #Compute x and y position derivatives for t=14
    x_prime.append(x_coor[7] - x_coor[6])
    y_prime.append(y_coor[7] - y_coor[6])
    
    for i in range(len(x_prime)):
        #Compute the speed values
        speed.append(np.sqrt(x_prime[i]**2 + y_prime[i]**2))
        
    return speed
    

# Problem 5
def jacobian_cdq2(f, x, h=1e-5):
    """Approximate the Jacobian matrix of f:R^n->R^m at x using the second
    order centered difference quotient.

    Parameters:
        f (function): the multidimensional function to differentiate.
            Accepts a NumPy (n,) ndarray and returns an (m,) ndarray.
            For example, f(x,y) = [x+y, xy**2] could be implemented as follows.
            >>> f = lambda x: np.array([x[0] + x[1], x[0] * x[1]**2])
        x ((n,) ndarray): the point in R^n at which to compute the Jacobian.
        h (float): the step size in the finite difference quotient.

    Returns:
        ((m,n) ndarray) the Jacobian matrix of f at x.
    """
    #Initialize identity matrix and list
    I, df_dxj = np.identity(len(x)), []
    
    for j in range(len(x)):
        
        #Compute Jacobian
        df_dxj.append((f(x + h*I[:, j]) - f(x - h*I[:, j]))/(2*h))
    return np.array(df_dxj).T
        

# Problem 6
def cheb_poly(x, n):
    """Compute the nth Chebyshev polynomial at x.

    Parameters:
        x (autograd.ndarray): the points to evaluate T_n(x) at.
        n (int): The degree of the polynomial.
    """
    #If n=0 then return an array of ones
    if n == 0:
        return anp.ones_like(x)
    
    #If n=1 then return x
    elif n == 1:
        return x
    
    #Otherwise keep recursively computing
    else:
        return 2 * x * cheb_poly(x, n-1) - cheb_poly(x, n-2)

def prob6():
    """Use Autograd and cheb_poly() to create a function for the derivative
    of the Chebyshev polynomials, and use that function to plot the derivatives
    over the domain [-1,1] for n=0,1,2,3,4.
    """
    #Create linspace and get derivative
    t, cheb = np.linspace(-1, 1, 100), elementwise_grad(cheb_poly)
    
    for n in [0,1,2,3,4]:
        
        #Plot
        plt.plot(t, cheb(t, n), label=f"n={n}")
        
    #Plot
    plt.xlabel("Domain")
    plt.ylabel("Derivative Value")
    plt.legend(loc='lower right')
    plt.title("Derivatives of Chebyshev Polynomials")
    plt.tight_layout()
    plt.show()

# Problem 7
def prob7(N=200):
    """Let f(x) = (sin(x) + 1)^sin(cos(x)). Perform the following experiment N
    times:

        1. Choose a random value x0.
        2. Use prob1() to calculate the “exact” value of f′(x0). Time how long
            the entire process takes, including calling prob1() (each
            iteration).
        3. Time how long it takes to get an approximation of f'(x0) using
            cdq4(). Record the absolute error of the approximation.
        4. Time how long it takes to get an approximation of f'(x0) using
            Autograd (calling grad() every time). Record the absolute error of
            the approximation.

    Plot the computation times versus the absolute errors on a log-log plot
    with different colors for SymPy, the difference quotient, and Autograd.
    For SymPy, assume an absolute error of 1e-18.
    """
    #Initialize lists to store time and functions
    symp_time, diff_time, auto_time, g = [], [], [], lambda x: (anp.sin(x) + 1)**(anp.sin(anp.cos(x)))
    diff_errors, auto_errors, x = [], [], sy.symbols("x")
    f = (sy.sin(x) + 1)**(sy.sin(sy.cos(x)))
    f = sy.lambdify(x, f, "numpy")
    
    for n in range(N):
        x0 = np.random.rand()
        
        #Compute the sympy method time
        start = time.time()
        df = prob1()
        d_prime1 = df(x0)
        symp_time.append(time.time() - start)
        
        #Compute the difference quotient time
        start = time.time()
        d_prime2 = cdq4(f, x0)
        diff_time.append(time.time() - start)
        
        #Compute the difference quotient time
        start = time.time()
        d_prime3 = grad(g)(x0)
        auto_time.append(time.time() - start)
        
        #Compute errors
        diff_errors.append(np.abs(d_prime1 - d_prime2))
        auto_errors.append(np.abs(d_prime1 - d_prime3))
        
    #Plot
    plt.scatter(symp_time, anp.ones_like(symp_time)*1e-18, alpha=0.4, label='SymPy')
    plt.scatter(diff_time, diff_errors, alpha=0.4, label='Diff Quotient')
    plt.scatter(auto_time, auto_errors, alpha=0.4, label='Autograd')
    plt.xlabel("Computation Time (seconds)")
    plt.xscale("log")
    plt.yscale("log")
    plt.ylabel("Absolute Error")
    plt.title(f"Efficiency of Differentiation Methods for N={N}")
    plt.show()
        
