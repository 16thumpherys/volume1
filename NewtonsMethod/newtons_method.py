# newtons_method.py
"""Volume 1: Newton's Method.
<Tyler Humpherys>
<Math 347>
<1/25/22>
"""
import numpy as np
from scipy import linalg as la
from matplotlib import pyplot as plt
from autograd import grad


# Problems 1, 3, and 5
def newton(f, x0, Df, tol=1e-5, maxiter=15, alpha=1.):
    """Use Newton's method to approximate a zero of the function f.

    Parameters:
        f (function): a function from R^n to R^n (assume n=1 until Problem 5).
        x0 (float or ndarray): The initial guess for the zero of f.
        Df (function): The derivative of f, a function from R^n to R^(nxn).
        tol (float): Convergence tolerance. The function should returns when
            the difference between successive approximations is less than tol.
        maxiter (int): The maximum number of iterations to compute.
        alpha (float): Backtracking scalar (Problem 3).

    Returns:
        (float or ndarray): The approximation for a zero of f.
        (bool): Whether or not Newton's method converged.
        (int): The number of iterations computed.
    """
    #Initialize status and iterations
    converge_status, iterations, xk = False, 0, x0
    
    #Check if x0 is a vector or scalar
    if not np.isscalar(x0):
        for i in range(maxiter):
            #Count a new iteration
            iterations += 1
            
            #Solve the linear system and compute xk+1
            yk = la.solve(Df(xk), f(xk))
            xk1 = xk - alpha * yk
            
            #Stop if the norm of the difference between the two is less than tol
            if la.norm(xk1 - xk) < tol:
                converge_status = True
                break
            
            #Switch variables to iterate
            xk = xk1
        
    else:
        for i in range(maxiter):
            #Count a new iteration
            iterations += 1
            
            #Compute xk+1
            xk1 = xk - alpha * f(xk)/Df(xk)
            
            #Stop if the absval of the difference between the two is less than tol
            if np.abs(xk1 - xk) < tol:
                converge_status = True
                break
            
            #Switch variables to iterate
            xk = xk1
        
    return xk1, converge_status, iterations


# Problem 2
def prob2(N1, N2, P1, P2):
    """Use Newton's method to solve for the constant r that satisfies

                P1[(1+r)**N1 - 1] = P2[1 - (1+r)**(-N2)].

    Use r_0 = 0.1 for the initial guess.

    Parameters:
        P1 (float): Amount of money deposited into account at the beginning of
            years 1, 2, ..., N1.
        P2 (float): Amount of money withdrawn at the beginning of years N1+1,
            N1+2, ..., N1+N2.
        N1 (int): Number of years money is deposited.
        N2 (int): Number of years money is withdrawn.

    Returns:
        (float): the value of r that satisfies the equation.
    """
    #Initialize function by putting both terms on one side
    f = lambda r: P1 * ((1+r)**N1 - 1) - (P2 * (1 - (1+r)**-N2))
    
    #Compute the derivative using Autograd
    df = grad(f)
    
    #Set 0.1 as initial guess for r
    r_0 = 0.1
    
    # Use Newton's method and return the r portion of the result
    r, converge_status, iterations = newton(f, r_0, df)
    
    return r


# Problem 4
def optimal_alpha(f, x0, Df, tol=1e-5, maxiter=15):
    """Run Newton's method for various values of alpha in (0,1].
    Plot the alpha value against the number of iterations until convergence.

    Parameters:
        f (function): a function from R^n to R^n (assume n=1 until Problem 5).
        x0 (float or ndarray): The initial guess for the zero of f.
        Df (function): The derivative of f, a function from R^n to R^(nxn).
        tol (float): Convergence tolerance. The function should returns when
            the difference between successive approximations is less than tol.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        (float): a value for alpha that results in the lowest number of
            iterations.
    """
    #Generate alphas in the given interval (0,1] and initialize list
    alpha_values, num_iterations = np.linspace(0.01, 1, 100), []
    
    for alpha in alpha_values:
        #Run newton's method on the alpha values and record their iterations
        xk1, converge_status, iterations = newton(f, x0, Df, tol, maxiter, alpha)
        num_iterations.append(iterations)
    
    #Plot
    plt.plot(alpha_values, num_iterations)
    plt.title('Iterations for Convergence')
    plt.xlabel('Alpha Value')
    plt.ylabel('Number of Iterations')
    plt.show()
    
    #Return the alpha that gives the lowest number of iterations
    return alpha_values[np.argmin(num_iterations)]


# Problem 6
def prob6():
    """Consider the following Bioremediation system.

                              5xy − x(1 + y) = 0
                        −xy + (1 − y)(1 + y) = 0

    Find an initial point such that Newton’s method converges to either
    (0,1) or (0,−1) with alpha = 1, and to (3.75, .25) with alpha = 0.55.
    Return the intial point as a 1-D NumPy array with 2 entries.
    """
    #Initialize the target points of convergence
    target1, target2, target3 = np.array([0,1]), np.array([0,-1]), np.array([3.75,.25])
    
    #Initialize f and compute its partial derivative
    f = lambda x: np.array([5 * x[0] * x[1] - x[0]*(1 + x[1]), -x[0]*x[1] + (1 - x[1]) * (1 + x[1])])
    df = lambda x: np.array([[4*x[1] - 1, 4 * x[0]], [-x[1], -x[0] - 2*x[1]]])
    
    #Find an initial point within the rectangle [-0.25, 0]x[0, 0.25]
    x_vals, y_vals = np.linspace(-0.25, 0, 100), np.linspace(0, 0.25, 100)
    for x0 in x_vals:
        for y0 in y_vals:
            #Create the canidate point to see if it converges
            canidate_point = np.array([x0, y0])
            
            #Use newton's method on the each (x0,y0)
            xk1, converge_status, iterations = newton(f, canidate_point, df)
            
            #If (x0,y0) converges to (0,1) or (0,-1)
            if np.allclose(target1, xk1) or np.allclose(target2, xk1):
                xk1, converge_status, iterations = newton(f, canidate_point, df, alpha=0.55)
                
                #If (x0,y0) converges to (3.75, .25) with alpha = 0.55 then return (x0,y0)
                if np.allclose(target3, xk1):
                    return canidate_point

# Problem 7
def plot_basins(f, Df, zeros, domain, res=1000, iters=15):
    """Plot the basins of attraction of f on the complex plane.

    Parameters:
        f (function): A function from C to C.
        Df (function): The derivative of f, a function from C to C.
        zeros (ndarray): A 1-D array of the zeros of f.
        domain ([r_min, r_max, i_min, i_max]): A list of scalars that define
            the window limits and grid domain for the plot.
        res (int): A scalar that determines the resolution of the plot.
            The visualized grid has shape (res, res).
        iters (int): The exact number of times to iterate Newton's method.
    """
    #Initialize real and imaginary parts
    x_real = np.linspace(domain[0], domain[1], res)
    x_imag = np.linspace(domain[2], domain[3], res)
    
    #Create an intial grid of complex points in this domain and combine the real and imaginary parts
    X_real, X_imag = np.meshgrid(x_real, x_imag)
    XK = X_real + 1j*X_imag
    
    for i in range(iters):
        #Use newton's method with no backtracking to compute XK1
        XK1 = XK - f(XK)/Df(XK)
        
        #Switch variables to iterate
        XK = XK1

    #Initialize the res x res array Y.
    Y_array = np.empty((res, res))

    #Build the array Y
    for i in range(res):
        for j in range(res):
            
            #Find the zero of f that is closest to the corresponding entry of XK
            Y_array[i,j] = np.argmin(np.abs(XK1[i,j] - zeros))

    #Plot
    plt.pcolormesh(X_real, X_imag, Y_array, cmap='brg')
    plt.title('Visualizing the Basins of Attraction for f(x)')
    plt.xlabel('Real parts of the grid')
    plt.ylabel('Imaginary parts of the grid')
    plt.show()

