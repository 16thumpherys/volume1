# solutions.py
"""Volume 1: The Page Rank Algorithm.
<Tyler Humpherys>
<Math 347>
<3/1/22>
"""
import numpy as np
import pandas as pd
import networkx as nx
from itertools import combinations


# Problems 1-2
class DiGraph:
    """A class for representing directed graphs via their adjacency matrices.

    Attributes:
        (fill this out after completing DiGraph.__init__().)
    """
    # Problem 1
    def __init__(self, A, labels=None):
        """Modify A so that there are no sinks in the corresponding graph,
        then calculate Ahat. Save Ahat and the labels as attributes.

        Parameters:
            A ((n,n) ndarray): the adjacency matrix of a directed graph.
                A[i,j] is the weight of the edge from node j to node i.
            labels (list(str)): labels for the n nodes in the graph.
                If None, defaults to [0, 1, ..., n-1].
        """
        #Save attributes
        self.A, self.node_labels = A, labels
        
        #If there are no node labels then label tham as integers
        if self.node_labels == None:
            self.node_labels = [i for i in range(A.shape[0])]
        
        #Make sure the numer of labels is equal to the number of nodes
        if len(self.node_labels) != self.A.shape[0]:
            raise ValueError("The number of labels isn't equal to the number of nodes in graph.")
            
        #Identify any sinks
        zero_col_status = ~A.any(axis=0)
        if zero_col_status.any():
            #Find sinks
            sinks = np.where(zero_col_status)[0]
            
            for index_zero_col in sinks:
                #If there are sinks replace them with all ones
                self.A[:, index_zero_col] = np.ones(len(self.A))
            
        #Compute A_hat
        col_sums = self.A.sum(axis=0)
        self.A_hat = self.A / col_sums[np.newaxis, :]
        

    # Problem 2
    def linsolve(self, epsilon=0.85):
        """Compute the PageRank vector using the linear system method.

        Parameters:
            epsilon (float): the damping factor, between 0 and 1.

        Returns:
            dict(str -> float): A dictionary mapping labels to PageRank values.
        """
        #Compute probabilities
        n = len(self.A)
        probabilities = np.linalg.solve((np.eye(n) - epsilon*self.A_hat), np.ones(n)*(1 - epsilon)/n)
        
        #Return the dectionary mapping labels to pagerank values
        return {self.node_labels[i]: val for i,val in enumerate(probabilities)}


    # Problem 2
    def eigensolve(self, epsilon=0.85):
        """Compute the PageRank vector using the eigenvalue method.
        Normalize the resulting eigenvector so its entries sum to 1.

        Parameters:
            epsilon (float): the damping factor, between 0 and 1.

        Return:
            dict(str -> float): A dictionary mapping labels to PageRank values.
        """
        #Create B matrix
        n = len(self.A)
        B = epsilon*self.A_hat + ((1 - epsilon)/n)*np.ones_like(self.A_hat)
        
        #Compute the eigenvalues and eigenvectors
        eigenvalues, eigenvectors = np.linalg.eig(B)
        
        #Compute the probabilities
        probabilities = eigenvectors[:, np.argsort(eigenvalues.real)[-1]].real
        probabilities = probabilities/sum(probabilities)
        
        #Return the dectionary mapping labels to pagerank values
        return {self.node_labels[i]: val for i,val in enumerate(probabilities)}


    # Problem 2
    def itersolve(self, epsilon=0.85, maxiter=100, tol=1e-12):
        """Compute the PageRank vector using the iterative method.

        Parameters:
            epsilon (float): the damping factor, between 0 and 1.
            maxiter (int): the maximum number of iterations to compute.
            tol (float): the convergence tolerance.

        Return:
            dict(str -> float): A dictionary mapping labels to PageRank values.
        """
        #Compute the initial probabilities
        n = len(self.A)
        P_curr = np.ones(n).T/n
        P_next = epsilon*self.A_hat@P_curr + (1 - epsilon)/np.ones(n)*n
        
        #Keep track of iterations
        iterations = 0
        
        #Iterate as long as these conditions are met:
        while (iterations <= maxiter) and (np.linalg.norm(P_next - P_curr, ord=1) >= tol):
            
            #Compute probabilities
            P_curr = P_next.copy()
            P_next = epsilon*(self.A_hat@P_curr) + (1 - epsilon)/np.ones(n)*n
            
            #Increase the iterations by 1
            iterations += 1
        
        #Return the dictionary
        probabilities = P_next/np.sum(P_next)
        return {self.node_labels[i]: val for i,val in enumerate(probabilities)}            


# Problem 3
def get_ranks(d):
    """Construct a sorted list of labels based on the PageRank vector.

    Parameters:
        d (dict(str -> float)): a dictionary mapping labels to PageRank values.

    Returns:
        (list) the keys of d, sorted by PageRank value from greatest to least.
    """
    #Create value and key tuples
    zipped = [(-val,key) for key,val in zip(d.keys(), d.values())]
    
    #Sort and return the keys of d sorted by PageRank value from greatest to least  
    zipped.sort()
    return [pair[1] for pair in zipped]


# Problem 4
def rank_websites(filename="web_stanford.txt", epsilon=0.85):
    """Read the specified file and construct a graph where node j points to
    node i if webpage j has a hyperlink to webpage i. Use the DiGraph class
    and its itersolve() method to compute the PageRank values of the webpages,
    then rank them with get_ranks(). If two webpages have the same rank,
    resolve ties by listing the webpage with the larger ID number first.

    Each line of the file has the format
        a/b/c/d/e/f...
    meaning the webpage with ID 'a' has hyperlinks to the webpages with IDs
    'b', 'c', 'd', and so on.

    Parameters:
        filename (str): the file to read from.
        epsilon (float): the damping factor, between 0 and 1.

    Returns:
        (list(str)): The ranked list of webpage IDs.
    """
    #Open file
    with open(filename, 'r') as file1:
        new_file1 = file1.read().strip()
    
    #Get the all the webpage names
    webpages = sorted(set(new_file1.replace('\n', '/').split('/')))
    
    #Make a dictionary for indexing purposes
    wd = {page:i for i,page in enumerate(webpages)}
    
    #Initialize the adjacency matrix
    Adj = np.zeros((len(webpages), len(webpages)))
    
    for file_line in new_file1.split('\n'):
        #Get all webpages and index
        sites = file_line.split('/')
        index = wd[sites[0]]
        
        for webpage in sites[1:]:
            #Update the adjacency matrix value for each connection
            Adj[wd[webpage], index] += 1
    
    #Return the ranked webpages
    return get_ranks(DiGraph(Adj, webpages).itersolve(epsilon))


# Problem 5
def rank_ncaa_teams(filename, epsilon=0.85):
    """Read the specified file and construct a graph where node j points to
    node i with weight w if team j was defeated by team i in w games. Use the
    DiGraph class and its itersolve() method to compute the PageRank values of
    the teams, then rank them with get_ranks().

    Each line of the file has the format
        A,B
    meaning team A defeated team B.

    Parameters:
        filename (str): the name of the data file to read.
        epsilon (float): the damping factor, between 0 and 1.

    Returns:
        (list(str)): The ranked list of team names.
    """
    #Get the names of the winners and losers
    win_names, lose_names = pd.read_csv(filename)['Winner'].to_list(), pd.read_csv(filename)['Loser'].to_list()
    
    #Get the name of unique teams
    unique_teams = list(np.unique(win_names + lose_names))
    
    #Initialize the adjacency matrix and create combos
    Adj, combos = np.zeros((len(unique_teams), len(unique_teams))), zip(win_names, lose_names)
    
    #Update the adjacency matrix value for each connection
    for win,lose in combos:
            Adj[unique_teams.index(win), unique_teams.index(lose)] += 1
    
    #Return the ranked teams
    return get_ranks(DiGraph(Adj, unique_teams).itersolve(epsilon))
    

# Problem 6
def rank_actors(filename="top250movies.txt", epsilon=0.85):
    """Read the specified file and construct a graph where node a points to
    node b with weight w if actor a and actor b were in w movies together but
    actor b was listed first. Use NetworkX to compute the PageRank values of
    the actors, then rank them with get_ranks().

    Each line of the file has the format
        title/actor1/actor2/actor3/...
    meaning actor2 and actor3 should each have an edge pointing to actor1,
    and actor3 should have an edge pointing to actor2.
    """
    #Create graph
    graph = nx.DiGraph()
    
    #Open the data
    with open(filename, 'r', encoding="utf-8") as file:
        data = [i for file_line in file.readlines() for i in file_line.strip().split("\n")]
    
    for title in data:
        #Grab the actors
        actors = (title.strip().split("/"))[1:]
        
        for name1, name2 in combinations(actors, 2):
            #Add one if there is an edge between the two actors
            if graph.has_edge(name2, name1):
                graph[name2][name1]["weight"] += 1
            
            #Otherwise
            else:
                graph.add_edge(name2, name1, weight=1)
    
    #Return the ranks
    return get_ranks(nx.pagerank(graph, alpha=epsilon))
    

