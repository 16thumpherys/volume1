# solutions.py
"""Volume 1: The SVD and Image Compression. Solutions File."""

import numpy as np
import scipy.linalg as la
from matplotlib import pyplot as plt
from imageio import imread

# Problem 1
def compact_svd(A, tol=1e-6):
    """Compute the truncated SVD of A.

    Parameters:
        A ((m,n) ndarray): The matrix (of rank r) to factor.
        tol (float): The tolerance for excluding singular values.

    Returns:
        ((m,r) ndarray): The orthonormal matrix U in the SVD.
        ((r,) ndarray): The singular values of A as a 1-D array.
        ((r,n) ndarray): The orthonormal matrix V^H in the SVD.
    """
    #Get A^H@A
    AHA = (A.conj().T)@A
    
    #Get the eigenvalues and eigenvectors 
    eigenvalues, eigenvecs = la.eig(AHA)
    
    #Get the singular values
    singular_vals = np.sqrt(eigenvalues)
    
    #Sort the eigenvectors and singular values from greatest to least
    index = np.argsort(singular_vals)[::-1]
    eigenvectors = eigenvecs[:, index]
    singular_values = singular_vals[index]
    
    #Count the number of nonzero singular values
    rank = len([i for i in singular_values if i>tol])
    
    #Keep the only positive singular values and their corresponding eigenvectors
    singular_values = singular_values[:rank]
    eigenvectors = eigenvectors[:,:rank]
    
    #Construct U with array broadcasting
    U = (A@eigenvectors)/singular_values
    
    return U, singular_values.real, eigenvectors.conj().T


# Problem 2
def visualize_svd(A):
    """Plot the effect of the SVD of A as a sequence of linear transformations
    on the unit circle and the two standard basis vectors.
    Parameters:
        A ((m,n), ndarray)
    Returns:
        Plots
    """
    #Get the SVD
    U, sigma, VH = la.svd(A)
    
    #Get the diagonals
    sigma = np.diag(sigma)
    E = np.array([[1,0,0],[0,0,1]])
    
    #Get the coordinates to plot
    x = np.cos(np.linspace(0, 2*np.pi, 200))
    y = np.sin(np.linspace(0, 2*np.pi, 200))
    S = np.vstack((x,y))
    
    #Create subplot for S only
    ax1 = plt.subplot(2,2,1)
    ax1.set_title("S only")
    ax1.plot(S[0,:], S[1,:])
    ax1.plot(E[0,:], E[1,:])
    ax1.set_aspect("equal")
    
    #Create subplot for VH@S
    value_S, value_E = VH@S, VH@E
    ax2 = plt.subplot(2,2,2)
    ax2.set_title("V^H@S")
    ax2.plot(value_S[0,:], value_S[1,:])
    ax2.plot(value_E[0,:], value_E[1,:])
    ax2.set_aspect("equal")
    
    #Create subplot for sigma@VH@S
    value_S, value_E = sigma@VH@S, sigma@VH@E
    ax3 = plt.subplot(2,2,3)
    ax3.set_title("Sigma@V^H@S")
    ax3.plot(value_S[0,:], value_S[1,:])
    ax3.plot(value_E[0,:], value_E[1,:])
    ax3.set_xlim(-5,5)
    ax3.set_ylim(-4,4)
    ax3.set_aspect("equal")
    
    #Create subplot for U@sigma@VH@S
    value_S, value_E = U@sigma@VH@S, U@sigma@VH@E
    ax4 = plt.subplot(2,2,4)
    ax4.set_title("U@Sigma@V^H@S")
    ax4.plot(value_S[0,:], value_S[1,:])
    ax4.plot(value_E[0,:], value_E[1,:])
    ax4.set_xlim(-4,4)
    ax4.set_ylim(-4,4)
    ax4.set_aspect("equal")
    
    plt.suptitle("Prob 2: Visualizing SVD")
    plt.tight_layout()
    plt.show()
    

# Problem 3
def svd_approx(A, s):
    """Return the best rank s approximation to A with respect to the 2-norm
    and the Frobenius norm, along with the number of bytes needed to store
    the approximation via the truncated SVD.

    Parameters:
        A ((m,n), ndarray)
        s (int): The rank of the desired approximation.

    Returns:
        ((m,n), ndarray) The best rank s approximation of A.
        (int) The number of entries needed to store the truncated SVD.
    """
    #If s is greater than the number of nonzero singular values of A 
    if s > np.linalg.matrix_rank(A):
        raise ValueError("s is greater than the rank(A)")
        
    #Get the SVD of A
    U, sigma, VH = la.svd(A)
    U, sigma, VH = U[:,:s], np.diag(sigma[:s]), VH[:s,:]
    
    #Calculate the number of entries required to store the truncated form
    entries = len(A)*s + s + len(A[0])*s
    
    #Recalculate the best rank s approximation A_s of A
    A_s = U@sigma@VH
    
    return A_s, entries


# Problem 4
def lowest_rank_approx(A, err):
    """Return the lowest rank approximation of A with error less than 'err'
    with respect to the matrix 2-norm, along with the number of bytes needed
    to store the approximation via the truncated SVD.

    Parameters:
        A ((m, n) ndarray)
        err (float): Desired maximum error.

    Returns:
        A_s ((m,n) ndarray) The lowest rank approximation of A satisfying
            ||A - A_s||_2 < err.
        (int) The number of entries needed to store the truncated SVD.
    """
    #Get the SVD of A
    U, sigma, VH = la.svd(A)
    
    #Compute s and check the error
    s = len(np.where(sigma > err)[0])
    if s == 0:
        raise ValueError("A cannot be approximated with matrix of lesser rank")
    
    #Compute the lowest rank approximation A_s of A
    U, sigma, VH = U[:,:s], np.diag(sigma[:s]), VH[:s,:]
    A_s = U@sigma@VH
    
    #Calculate the number of entries required to store the truncated form
    entries = len(A)*s + s + len(A[0])*s
    
    return A_s, entries


# Problem 5
def compress_image(filename, s):
    """Plot the original image found at 'filename' and the rank s approximation
    of the image found at 'filename.' State in the figure title the difference
    in the number of entries used to store the original image and the
    approximation.

    Parameters:
        filename (str): Image file path.
        s (int): Rank of new image.
    """
    #Read in the image
    image = imread(filename)/255
    
    #If the image is color
    if len(image.shape)==3:
        #Get the original number of entries
        ori_num_entries = image.size
        
        #Get the SVD_approx for each channel
        Rs, num1 = svd_approx(image[:,:,0], s)
        Gs, num2 = svd_approx(image[:,:,1], s)
        Bs, num3 = svd_approx(image[:,:,2], s)
        
        #Plot the original image
        plt.subplot(121)
        plt.imshow(image)
        plt.title("Original")
        
        #Edit image
        image = np.dstack((Rs, Gs, Bs))
        image = np.clip(image, 0, 1)
        
        #Plot the changed image
        plt.subplot(122)
        plt.imshow(image)
        plt.title("Lowrank approx")
        plt.axis("off")
        
        #Report entries saved
        plt.suptitle(str(ori_num_entries - (num1+num2+num3)) + " entries saved")
        plt.show()
        
    #Otherwise it is black and white
    else:
        #Get the original number of entries
        ori_num_entries = len(image)*len(image[0])
        
        #Plot the original image
        plt.subplot(121)
        plt.imshow(image, cmap='gray')
        plt.title("Original")
        
        #Get the new approx and edit image
        image, num_entries = svd_approx(image, s)
        image = np.clip(image, 0, 1)
        
        #Plot the changed image
        plt.subplot(122)
        plt.imshow(image, cmap='gray')
        plt.title("Lowrank approx")
        plt.axis("off")
        
        #Report the entries saved
        plt.suptitle(str(ori_num_entries - num_entries) + " entries saved")
        plt.show()
