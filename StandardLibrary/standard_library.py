# standard_library.py
"""Python Essentials: The Standard Library.
<Tyler Humpherys>
<Math 345>
<9/7/21>
"""
import calculator as cl
from itertools import combinations
import math
import random
import time
import box
import sys

# Problem 1
def prob1(L):
    """Return the minimum, maximum, and average of the entries of L
    (in that order).
    """
    return min(L), max(L), (sum(L)/len(L))


# Problem 2
def prob2():
    """Determine which Python objects are mutable and which are immutable.
    Test numbers, strings, lists, tuples, and sets. Print your results.
    """
    int_1 = 1
    int_2 = int_1
    int_2 = 4
    print("int object:", int_1 == int_2, "= immutable")
    str_1 = "Hello"
    str_2 = str_1
    str_2 += "Hi"
    print("str object:", str_1 == str_2, "= immutable")
    list_1 = [1,2,3,4,5]
    list_2 = list_1
    list_2.append(3)
    print("list object:", list_1 == list_2, "= mutable")
    tuple_1 = (1,2)
    tuple_2 = tuple_1
    tuple_2 += (1,)
    print("tuple object:", tuple_1 == tuple_2, "= immutable")
    set_1 = {1,2}
    set_2 = set_1
    set_2.add(3)
    print("set object:", set_1 == set_2, "= mutable")
    

# Problem 3
def hypot(a, b):
    """Calculate and return the length of the hypotenuse of a right triangle.
    Do not use any functions other than sum(), product() and sqrt that are 
    imported from your 'calculator' module.

    Parameters:
        a: the length one of the sides of the triangle.
        b: the length the other non-hypotenuse side of the triangle.
    Returns:
        The length of the triangle's hypotenuse.
    """
    return cl.sqrt(cl.sum(cl.product(a, a), cl.product(b, b)))


# Problem 4
def power_set(A):
    """Use itertools to compute the power set of A.

    Parameters:
        A (iterable): a str, list, set, tuple, or other iterable collection.

    Returns:
        (list(sets)): The power set of A as a list of sets.
    """
    ps = []
    for i in range(len(A) + 1):
        #Create a new subset
        newsubsets = [set(z) for z in combinations(A, i)]
        
        #Put new subset into my powerset
        ps.extend(newsubsets)
    return ps


# Problem 5: Implement shut the box.
def shut_the_box(player, timelimit):
    """Play a single game of shut the box."""
    
    #Set initial conditions
    nums_left = [1,2,3,4,5,6,7,8,9]
    status = True
    start = time.time()
    end = timelimit + time.time()
    
    while (status == True):
        
        #Tells user what numbers are left
        print("Numbers left:", nums_left)
        
        #Roll the dice. If sum of remaining numbers is less than 6 then roll 
        #only one dice.
        if sum(nums_left) > 6:
            roll = random.randint(2,12)
        else:
            roll = random.randint(1,6)
        
        #Check if my remaining numbers can't add up to my roll
        if (box.isvalid(roll, nums_left) == False):
            print('Roll: ', roll)
            print("Game over!")
            print()
            print("Score for player " + str(player) + ": " + str(sum(nums_left)) + " points")
            time_played = round(time.time() - start, ndigits=2)
            print("Time played: " + str(time_played) + " seconds")
            print("Better luck next time >:)")
            break
            
        #Give user roll and time information.
        print("Roll: ", roll)
        time_played = round(time.time() - start, ndigits=2)
        print("Seconds left: ", round(timelimit - time_played, ndigits=2), end='')
        
        #Check if there is still time
        if time.time() >= end:
            print("Score for player " + str(player) + ": " + str(sum(nums_left)) + " points")
            print("Time played: " + str(time_played) + " seconds")
            print("Better luck next time >:)")
            break
        
        #Check if eliminating these numbers is a valid move
        valid_move = False
        while valid_move == False:
            valid_move = True
            
            #Ask for numbers to eliminate            
            input_string = input("Numbers to eliminate: ")
            input_string_parsed = box.parse_input(input_string, nums_left)
            if input_string_parsed == []:
                print("Invalid input")
                valid_move = False
            else:
                numbers = [int(num) for num in input_string.split()]
                if sum(numbers) != roll:
                    print("Invalid input")
                    valid_move = False
                for num in numbers:
                    if num not in nums_left:
                        print("Invalid input")
                        valid_move = False
            
        #Eliminate the selected numbers from list.
        for num in numbers:
            nums_left.remove(num)
             
        #Update game status to determine if another turn should be taken.
        if (len(nums_left) == 0) or (time.time() >= end):
            #Stop the game
            status = False
            time_played = round(time.time() - start, ndigits=2)
            
            #Determine what message to display 
            if len(nums_left) == 0:
                print()
                print("Score for player " + str(player) + ": " + str(sum(nums_left)) + " points")
                print("Time played: " + str(time_played) + " seconds")
                print("Congratulations!! You shut the box!")
                
            if time.time() >= end:
                print("Game over!")
                print()
                print("Score for player " + str(player) + ": " + str(sum(nums_left)) + " points")
                print("Time played: " + str(time_played) + " seconds")
                print("Better luck next time >:)")
                


"""The code below ensures that three arguments are provided when the file is
run. If not, then the user is told they did not provide enough arguments."""
if len(sys.argv) == 3:
    player_name = sys.argv[1]
    time_limit = int(sys.argv[2])
    shut_the_box(player_name, time_limit)
else:
    print("You did not provide enough arguments to play shut the box.")
    