#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  7 09:44:01 2021

@author: tylerhumpherys
"""
from math import sqrt

def sum(a, b):
    return a + b

def product(a, b):
    return a*b
