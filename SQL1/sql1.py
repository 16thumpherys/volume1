# sql1.py
"""Volume 1: SQL 1 (Introduction).
<Tyler Humpherys>
<Math 347>
<3/22/22>
"""
import sqlite3 as sql
import csv
import numpy as np
from matplotlib import pyplot as plt

# Problems 1, 2, and 4
def student_db(db_file="students.db", student_info="student_info.csv",
                                      student_grades="student_grades.csv"):
    """Connect to the database db_file (or create it if it doesn’t exist).
    Drop the tables MajorInfo, CourseInfo, StudentInfo, and StudentGrades from
    the database (if they exist). Recreate the following (empty) tables in the
    database with the specified columns.

        - MajorInfo: MajorID (integers) and MajorName (strings).
        - CourseInfo: CourseID (integers) and CourseName (strings).
        - StudentInfo: StudentID (integers), StudentName (strings), and
            MajorID (integers).
        - StudentGrades: StudentID (integers), CourseID (integers), and
            Grade (strings).

    Next, populate the new tables with the following data and the data in
    the specified 'student_info' 'student_grades' files.

                MajorInfo                         CourseInfo
            MajorID | MajorName               CourseID | CourseName
            -------------------               ---------------------
                1   | Math                        1    | Calculus
                2   | Science                     2    | English
                3   | Writing                     3    | Pottery
                4   | Art                         4    | History

    Finally, in the StudentInfo table, replace values of −1 in the MajorID
    column with NULL values.

    Parameters:
        db_file (str): The name of the database file.
        student_info (str): The name of a csv file containing data for the
            StudentInfo table.
        student_grades (str): The name of a csv file containing data for the
            StudentGrades table.
    """
    #Create lists containing the tuples for MajorInfo and CourseInfo tables
    Major_rows = [(1, 'Math'), (2, 'Science'), (3, 'Writing'), (4, 'Art')]
    Course_rows = [(1, 'Calculus'), (2, 'English'), (3, 'Pottery'), (4, 'History')]
    
    #Open the student_info file
    with open(student_info, 'r') as infile:        
        #Grab the rows and insert into table
        Student_rows = list(csv.reader(infile))
            
    #Open the student_grades file
    with open(student_grades, 'r') as infile:
        #Grab the rows and insert into table
        Grades_rows = list(csv.reader(infile))
        
    try:
        #Connect to the database (create if it doesn't exist)
        with sql.connect(db_file) as conn:
            cur = conn.cursor()
            
            #Drop the tables if they exist
            cur.execute("DROP TABLE IF EXISTS MajorInfo;")
            cur.execute("DROP TABLE IF EXISTS CourseInfo;")
            cur.execute("DROP TABLE IF EXISTS StudentInfo;")
            cur.execute("DROP TABLE IF EXISTS StudentGrades;")
            
            #Add the following tables
            cur.execute("CREATE TABLE MajorInfo (MajorID INTGER, MajorName TEXT);")
            cur.execute("CREATE TABLE CourseInfo (CourseID INTGER, CourseName TEXT);")
            cur.execute("CREATE TABLE StudentInfo (StudentID INTGER, StudentName TEXT, MajorID INTEGER);")
            cur.execute("CREATE TABLE StudentGrades (StudentID INTGER, CourseID INTGER, Grade TEXT);")
            
            #Insert information
            cur.executemany("INSERT INTO MajorInfo VALUES(?,?);", Major_rows)
            cur.executemany("INSERT INTO CourseInfo VALUES(?,?);", Course_rows)
            cur.executemany("INSERT INTO StudentInfo VALUES(?,?,?);", Student_rows)
            cur.executemany("INSERT INTO StudentGrades VALUES(?,?,?);", Grades_rows)
            
            #Change -1 values to null in StudentInfo table
            cur.execute("UPDATE StudentInfo SET MajorID = NULL WHERE MajorID == -1;")

    finally:
        #Close the database
        conn.close()


# Problems 3 and 4
def earthquakes_db(db_file="earthquakes.db", data_file="us_earthquakes.csv"):
    """Connect to the database db_file (or create it if it doesn’t exist).
    Drop the USEarthquakes table if it already exists, then create a new
    USEarthquakes table with schema
    (Year, Month, Day, Hour, Minute, Second, Latitude, Longitude, Magnitude).
    Populate the table with the data from 'data_file'.

    For the Minute, Hour, Second, and Day columns in the USEarthquakes table,
    change all zero values to NULL. These are values where the data originally
    was not provided.

    Parameters:
        db_file (str): The name of the database file.
        data_file (str): The name of a csv file containing data for the
            USEarthquakes table.
    """    
    #Open the data file
    with open(data_file, 'r') as infile:        
        #Grab the rows and insert into table
        eq_rows = list(csv.reader(infile))
        
    try:
        #Connect to the database (create if it doesn't exist)
        with sql.connect(db_file) as conn:
            cur = conn.cursor()
            
            #Drop the tables if they exist
            cur.execute("DROP TABLE IF EXISTS USEarthquakes;")
            
            #Add the following tables
            cur.execute("CREATE TABLE USEarthquakes (Year INTGER, Month INTEGER, Day INTEGER, Hour INTEGER, Minute INTEGER, Second INTEGER, Latitude REAL, Longitude REAL, Magnitude REAL);")

            #Insert information
            cur.executemany("INSERT INTO USEarthquakes VALUES(?,?,?,?,?,?,?,?,?);", eq_rows)
            
            #Delete rows from USEarthquakes table have a value of 0 for magnitude
            cur.execute("DELETE FROM USEarthquakes WHERE Magnitude == 0;")
            
            #Change 0 values for day, hour, minute, or second to null in USEarthquakes table
            cur.execute("UPDATE USEarthquakes SET Day = NULL WHERE Day == 0;")
            cur.execute("UPDATE USEarthquakes SET Hour = NULL WHERE Hour == 0;")
            cur.execute("UPDATE USEarthquakes SET Minute = NULL WHERE Minute == 0;")
            cur.execute("UPDATE USEarthquakes SET Second = NULL WHERE Second == 0;")

    finally:
        #Close the database
        conn.close()


# Problem 5
def prob5(db_file="students.db"):
    """Query the database for all tuples of the form (StudentName, CourseName)
    where that student has an 'A' or 'A+'' grade in that course. Return the
    list of tuples.

    Parameters:
        db_file (str): the name of the database to connect to.

    Returns:
        (list): the complete result set for the query.
    """
    #Connect to the database
    conn = sql.connect(db_file)
    cur = conn.cursor()
    
    #Find the tuples of the given criteria
    cur.execute("SELECT SI.StudentName, CI.CourseName FROM StudentInfo AS SI, CourseInfo AS CI, StudentGrades AS SG WHERE SI.StudentID == SG.StudentID AND CI.CourseID == SG.CourseID AND Grade IN ('A+', 'A');")

    #Fetchall
    all_tups = cur.fetchall()
    
    #Close the database and return the tuples
    conn.close()
    return all_tups


# Problem 6
def prob6(db_file="earthquakes.db"):
    """Create a single figure with two subplots: a histogram of the magnitudes
    of the earthquakes from 1800-1900, and a histogram of the magnitudes of the
    earthquakes from 1900-2000. Also calculate and return the average magnitude
    of all of the earthquakes in the database.

    Parameters:
        db_file (str): the name of the database to connect to.

    Returns:
        (float): The average magnitude of all earthquakes in the database.
    """
    #Connect to the database
    conn = sql.connect(db_file)
    cur = conn.cursor()
    
    #Get the magnitudes of the earthquakes during 19th century
    cur.execute("SELECT EQ.Magnitude FROM USEarthquakes AS EQ WHERE Year >= 1800 AND Year <= 1899;")
    mags_19th_cent = np.ravel(cur.fetchall())

    #Get the magnitudes of the earthquakes during 19th century
    cur.execute("SELECT EQ.Magnitude FROM USEarthquakes AS EQ WHERE Year >= 1900 AND Year <= 1999;")
    mags_20th_cent = np.ravel(cur.fetchall())
    
    #Get the magnitudes of the earthquakes during 19th century
    cur.execute("SELECT AVG(EQ.Magnitude) FROM USEarthquakes AS EQ;")
    avg = cur.fetchall()[0][0]
    
    #Close the database and return the tuples
    conn.close()
    
    #Plot
    plt.subplot(121)
    plt.hist(mags_19th_cent)
    plt.xlabel("Magnitude")
    plt.ylabel("Count")
    plt.title("Magnitudes of 19th Century")
    plt.subplot(122)
    plt.hist(mags_20th_cent)
    plt.xlabel("Magnitude")
    plt.title("Magnitudes of 20th Century")
    plt.suptitle("Earthquake Magnitudes by Century")
    plt.show()
    
    return avg
    
