# montecarlo_integration.py
"""Volume 1: Monte Carlo Integration.
<Tyler Humpherys>
<Math 347>
<2/14/22>
"""
import numpy as np
from scipy import linalg as la
from scipy import stats
from matplotlib import pyplot as plt

# Problem 1
def ball_volume(n, N=10000):
    """Estimate the volume of the n-dimensional unit ball.

    Parameters:
        n (int): The dimension of the ball. n=2 corresponds to the unit circle,
            n=3 corresponds to the unit sphere, and so on.
        N (int): The number of random points to sample.

    Returns:
        (float): An estimate for the volume of the n-dimensional unit ball.
    """
    #Get random points in the domain
    points = np.random.uniform(-1, 1, (n, N))
    
    #Determine how many points are within the circle
    lengths = la.norm(points, axis=0)
    num_within = np.count_nonzero(lengths < 1)
    
    #Estimate the volume
    return 2**n * (num_within / N)

# Problem 2
def mc_integrate1d(f, a, b, N=10000):
    """Approximate the integral of f on the interval [a,b].

    Parameters:
        f (function): the function to integrate. Accepts and returns scalars.
        a (float): the lower bound of interval of integration.
        b (float): the lower bound of interval of integration.
        N (int): The number of random points to sample.

    Returns:
        (float): An approximation of the integral of f over [a,b].

    Example:
        >>> f = lambda x: x**2
        >>> mc_integrate1d(f, -4, 2)    # Integrate from -4 to 2.
        23.734810301138324              # The true value is 24.
    """
    #Get random points
    points = np.random.uniform(a, b, N)
    
    #Find Volume * 1/N
    value, integrationSum = (b-a) * (1/N), 0
    
    #Find summation of function
    for point in points:
        integrationSum += f(point)
    return value * integrationSum


# Problem 3
def mc_integrate(f, mins, maxs, N=10000):
    """Approximate the integral of f over the box defined by mins and maxs.

    Parameters:
        f (function): The function to integrate. Accepts and returns
            1-D NumPy arrays of length n.
        mins (list): the lower bounds of integration.
        maxs (list): the upper bounds of integration.
        N (int): The number of random points to sample.

    Returns:
        (float): An approximation of the integral of f over the domain.

    Example:
        # Define f(x,y) = 3x - 4y + y^2. Inputs are grouped into an array.
        >>> f = lambda x: 3*x[0] - 4*x[1] + x[1]**2

        # Integrate over the box [1,3]x[-2,1].
        >>> mc_integrate(f, [1, -2], [3, 1])
        53.562651072181225              # The true value is 54.
    """
    #Convert to arrays
    a = np.array(mins)
    b = np.array(maxs)
    
    #Take the product
    product = np.prod(b-a)
    
    #Get points
    newArray = np.random.uniform(0, 1, (len(a), N)).T*(b-a) + a
    
    #Stretch and shift N times
    total = 0
    for i in range(N):
        total += f(newArray[i])
    return float(product*total/N)


# Problem 4
def prob4():
    """Let n=4 and Omega = [-3/2,3/4]x[0,1]x[0,1/2]x[0,1].
    - Define the joint distribution f of n standard normal random variables.
    - Use SciPy to integrate f over Omega.
    - Get 20 integer values of N that are roughly logarithmically spaced from
        10**1 to 10**5. For each value of N, use mc_integrate() to compute
        estimates of the integral of f over Omega with N samples. Compute the
        relative error of estimate.
    - Plot the relative error against the sample size N on a log-log scale.
        Also plot the line 1 / sqrt(N) for comparison.
    """
    #Initialize domain
    a, b, n = np.array([-3/2, 0, 0, 0]), np.array([3/4, 1, 1/2, 1]), 4
    
    #Initialize function
    f = lambda x: (1/(2*np.pi)**(n/2))*np.exp((x.T@x)/-2)
    
    #The distribution has mean 0 and covariance I (the nxn identity).
    means, cov = np.zeros(4), np.eye(4)
    
    #Compute the integral with SciPy.
    actual = stats.mvn.mvnun(a, b, means, cov)[0]
    
    #Get integer values that are logarithmically spaced
    logs = np.logspace(1, 5, 20)
    
    #Get approximation
    errors = []
    for log in logs:
        
        #Use problem 3 to approximate and compute the difference as error
        approx_val = mc_integrate(f, a, b, int(np.floor(log)))
        errors.append(np.abs(approx_val - actual)/ np.abs(actual))
        
    #Plot
    plt.loglog(logs, errors, label='Relative Error')
    plt.loglog(logs, 1/np.sqrt(logs), label='1/sqrt(N)')
    plt.legend(loc='upper right')
    plt.xlabel('N')
    plt.ylabel('Error')
    plt.title('Error of Density Function')
    plt.show()
    