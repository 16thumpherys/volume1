# image_segmentation.py
"""Volume 1: Image Segmentation.
<Tyler Humpherys>
<Math 345>
<11/2/21>
"""

import numpy as np
from scipy import linalg as la
from imageio import imread
from matplotlib import pyplot as plt
from scipy.sparse import csc_matrix, lil_matrix, diags, csgraph, linalg

# Problem 1
def laplacian(A):
    """Compute the Laplacian matrix of the graph G that has adjacency matrix A.

    Parameters:
        A ((N,N) ndarray): The adjacency matrix of an undirected graph G.

    Returns:
        L ((N,N) ndarray): The Laplacian matrix of G.
    """
    #Collect the sums of all the rows and put into a diagonal matrix
    row_sums = np.sum(A, axis=1)
    #Compute L= D - A
    return np.diag(row_sums) - A
    

# Problem 2
def connectivity(A, tol=1e-8):
    """Compute the number of connected components in the graph G and its
    algebraic connectivity, given the adjacency matrix A of G.

    Parameters:
        A ((N,N) ndarray): The adjacency matrix of an undirected graph G.
        tol (float): Eigenvalues that are less than this tolerance are
            considered zero.

    Returns:
        (int): The number of connected components in G.
        (float): the algebraic connectivity of G.
    """
    #Compute the eigenvalues of the laplacian matrix of A
    eigs = np.sort(np.real(la.eigvals(laplacian(A))))
    
    #Count the number of eigenvalues that are zero
    count = 0
    for i in range(len(eigs)):
        #Wanted eigenvalues will be less than the tolerance (close to 0)
        if eigs[i] < tol:
            count += 1
    return count, eigs[1]

# Helper function for problem 4.
def get_neighbors(index, radius, height, width):
    """Calculate the flattened indices of the pixels that are within the given
    distance of a central pixel, and their distances from the central pixel.

    Parameters:
        index (int): The index of a central pixel in a flattened image array
            with original shape (radius, height).
        radius (float): Radius of the neighborhood around the central pixel.
        height (int): The height of the original image in pixels.
        width (int): The width of the original image in pixels.

    Returns:
        (1-D ndarray): the indices of the pixels that are within the specified
            radius of the central pixel, with respect to the flattened image.
        (1-D ndarray): the euclidean distances from the neighborhood pixels to
            the central pixel.
    """
    # Calculate the original 2-D coordinates of the central pixel.
    row, col = index // width, index % width

    # Get a grid of possible candidates that are close to the central pixel.
    r = int(radius)
    x = np.arange(max(col - r, 0), min(col + r + 1, width))
    y = np.arange(max(row - r, 0), min(row + r + 1, height))
    X, Y = np.meshgrid(x, y)

    # Determine which candidates are within the given radius of the pixel.
    R = np.sqrt(((X - col)**2 + (Y - row)**2))
    mask = R < radius
    return (X[mask] + Y[mask]*width).astype(np.int), R[mask]


# Problems 3-6
class ImageSegmenter:
    """Class for storing and segmenting images."""

    # Problem 3
    def __init__(self, filename):
        """Read the image file. Store its brightness values as a flat array."""
        #Read the file
        new_image = imread(filename)
            
        #Scale the image to floats between 0 and 1 for Matplotlib
        new_image = new_image/255
        #Store the scaled image as an attibute
        self.scaled_img = new_image
        
        #Check if imgae is in color
        if self.scaled_img.ndim == 3:
            #Average the RGB values of the colored image to get grayscale
            brightness = self.scaled_img.mean(axis=2)
            
            #Flatten to 1D array
            self.flat_bright = np.ravel(brightness)
        
        #Otherwise it is black and white
        else:
            #Flatten to 1D array
            self.flat_bright = np.ravel(self.scaled_img)

    # Problem 3
    def show_original(self):
        """Display the original image."""
        #Check if image is in color
        if self.scaled_img.ndim == 3:
            plt.imshow(self.scaled_img)
            plt.axis("off")
        
        #Otherwise show in gray
        else:
            plt.imshow(self.scaled_img, cmap="gray")
            plt.axis("off")
            
        plt.show()

    # Problem 4
    def adjacency(self, r=5., sigma_B2=.02, sigma_X2=3.):
        """Compute the Adjacency and Degree matrices for the image graph."""
        #Check if imgae is in color
        if self.scaled_img.ndim == 3:
            m, n, x = self.scaled_img.shape
        
        #Otherwise
        else:
            m, n = self.scaled_img.shape
        
        #Initialize A and D
        A = lil_matrix((m*n, m*n))
        D = np.zeros(m*n)
        
        for i in range(m*n):
            #For all indicies get the index of the neighbors and their distances
            index_of_neighbors, distances = get_neighbors(i, r, m, n)
            weights_list = []
            
            #Calculate the weights
            for index, k in zip(index_of_neighbors, distances):
                #distance = distances[index]
                
                #Compute the weight
                if k < r:
                    weight = np.exp(((-1*np.abs(self.flat_bright[i] - self.flat_bright[index])/sigma_B2)) - (k/sigma_X2))
                    
                else:
                    weight = 0
                
                #Add the weight to the list of weights
                weights_list.append(weight)
                
            #Finish making D and A
            D[i] = np.sum(weights_list)
            A[i, index_of_neighbors] = weights_list
        
        #Convert A to csc matrix
        A = csc_matrix(A)
        return A, D

    # Problem 5
    def cut(self, A, D):
        """Compute the boolean mask that segments the image."""
        #Get the dimensions
        #Check if image is in color
        if self.scaled_img.ndim == 3:
            m, n, x = self.scaled_img.shape
        
        #Otherwise
        else:
            m, n = self.scaled_img.shape
        
        #Compute the Laplacian
        L = csgraph.laplacian(A)
        
        #Compute D to the negative 0.5 and take the sparse matrix
        D_negSqrt = diags(D**(-0.5))
        
        #Compute D_negSqrt@L@D_negSqrt
        value = D_negSqrt@L@D_negSqrt
        
        #Compute the eigenvector
        eigvals, eigvecs = linalg.eigsh(value, which="SM", k=2)
        eigvec = eigvecs[:, 1]
        
        #Reshape the eigenvector as mxn and construct the boolean mask
        mask = np.reshape(eigvec, (m, n)) > 0
        
        return mask

    # Problem 6
    def segment(self, r=5., sigma_B=.02, sigma_X=3.):
        """Display the original image and its segments."""
        #Get A and D
        A, D = self.adjacency(r, sigma_B, sigma_X)
        
        #Get the mask
        mask = self.cut(A, D)
        
        #Check if image is in color
        if self.scaled_img.ndim == 3:
            mask = np.dstack((mask,mask,mask))
            #Multiply the mask by the original image array
            positive_segment = np.multiply(self.scaled_img, mask)
            negative_segment = np.multiply(self.scaled_img, ~mask)
            
            #Plot original image in color
            ax1 = plt.subplot(131)
            ax1.imshow(self.scaled_img)
            plt.title("Original Image")
            ax1.axis("off")
            
            #Plot positive image in color
            ax2 = plt.subplot(132)
            ax2.imshow(positive_segment)
            plt.title("Positive Segment")
            ax2.axis("off")
            
            #Plot negative image in color
            ax3 = plt.subplot(133)
            ax3.imshow(negative_segment)
            plt.title("Negative Segment")
            ax3.axis("off")
        
        #Otherwise
        else:
            #Multiply the mask by the original image array
            positive_segment = np.multiply(self.scaled_img, mask)
            negative_segment = np.multiply(self.scaled_img, ~mask)
            
            #Plot original image in gray
            ax1 = plt.subplot(131)
            ax1.imshow(self.scaled_img, cmap="gray")
            plt.title("Original Image")
            ax1.axis("off")
            
            #Plot positive image in gray
            ax2 = plt.subplot(132)
            ax2.imshow(positive_segment, cmap="gray")
            plt.title("Positive Segment")
            ax2.axis("off")
            
            #Plot negative image in gray
            ax3 = plt.subplot(133)
            ax3.imshow(negative_segment, cmap="gray")
            plt.title("Negative Segment")
            ax3.axis("off")
            
        plt.show()

