# matplotlib_intro.py
"""Python Essentials: Intro to Matplotlib.
<Tyler Humpherys>
<Math 345>
<9/14/21>
"""
import numpy as np
import random
from matplotlib import pyplot as plt
from math import sin, cos, atan, tan

# Problem 1
def var_of_means(n):
    """Construct a random matrix A with values drawn from the standard normal
    distribution. Calculate the mean value of each row, then calculate the
    variance of these means. Return the variance.

    Parameters:
        n (int): The number of rows and columns in the matrix A.

    Returns:
        (float) The variance of the means of each row.
    """
    #Create matrix with random values from a standard normal distribution
    A = np.random.normal(size=(n,n))
    #Take the mean of each row of the matrix A
    means = np.mean(A, axis=1)
    #Return the variance of the means
    return np.var(means)
    

def prob1():
    """Create an array of the results of var_of_means() with inputs
    n = 100, 200, ..., 1000. Plot and show the resulting array.
    """
    values = [n for n in range(100, 1000, 100)]
    #Take the variance of the means for n=100, ..., 1000 and put into array
    results = np.array([var_of_means(n) for n in values])
    #Plot the results
    x = values
    y = results
    plt.plot(x, y)
    plt.show()
    

# Problem 2
def prob2():
    """Plot the functions sin(x), cos(x), and arctan(x) on the domain
    [-2pi, 2pi]. Make sure the domain is refined enough to produce a figure
    with good resolution.
    """
    #Create x values to plug into function
    x = np.linspace(-2*np.pi, 2*np.pi, 70)
    
    #Create functions
    Sin = np.array(list(map(sin, x)))
    Cos = np.array(list(map(cos, x)))
    Atan = np.array(list(map(atan, x)))
    
    #Plot functions with figure title and legends and names for functions
    plt.plot(x, Sin, label = "Sin(x)")
    plt.plot(x, Cos, label = "Cos(x)")
    plt.plot(x, Atan, label = "Arctan(x)")
    plt.xlim(-2*np.pi, 2*np.pi) 
    plt.xlabel("x-axis")
    plt.ylabel("y-axis")
    plt.title("Problem 2")
    plt.legend(loc = "upper left")
    plt.show()


# Problem 3
def prob3():
    """Plot the curve f(x) = 1/(x-1) on the domain [-2,6].
        1. Split the domain so that the curve looks discontinuous.
        2. Plot both curves with a thick, dashed magenta line.
        3. Set the range of the x-axis to [-2,6] and the range of the
           y-axis to [-6,6].
    """
    #Create function
    g = lambda x: (1/(x-1))

    #Create x values to plug into function
    x1 = np.linspace(-2, 1, 70)
    x2 = np.linspace(1, 6, 70)
    
    #Create y values
    y1 = np.array(list(map(g, x1)))
    y2 = np.array(list(map(g, x2)))
    
    #Plot x and y values as a thick, dashed magenta line
    plt.plot(x1, y1, 'm--', lw=4, label = "f(x) = 1/(x-1)")
    plt.plot(x2, y2, 'm--', lw=4)
    plt.xlim(-2, 6)
    plt.ylim(-6, 6)
    plt.xlabel("x-axis")
    plt.ylabel("y-axis")
    plt.title("Problem 3")
    plt.legend(loc = "upper left")
    plt.show()


# Problem 4
def prob4():
    """Plot the functions sin(x), sin(2x), 2sin(x), and 2sin(2x) on the
    domain [0, 2pi].
        1. Arrange the plots in a square grid of four subplots.
        2. Set the limits of each subplot to [0, 2pi]x[-2, 2].
        3. Give each subplot an appropriate title.
        4. Give the overall figure a title.
        5. Use the following line colors and styles.
              sin(x): green solid line.
             sin(2x): red dashed line.
             2sin(x): blue dashed line.
            2sin(2x): magenta dotted line.
    """
    #Create input values x
    x = np.linspace(0, 2*np.pi, 100)
    
    #Create subplots
    plt.subplot(221)
    plt.plot(x, np.sin(x), 'g-')
    plt.axis([0, 2*np.pi, -2, 2])
    plt.title("sin(x)", fontsize=8)
    
    plt.subplot(222)
    plt.plot(x, np.sin(2*x), 'r--')
    plt.axis([0, 2*np.pi, -2, 2])
    plt.title("sin(2x)", fontsize=8)
    
    plt.subplot(223)
    plt.plot(x, 2*np.sin(x), 'b--')
    plt.axis([0, 2*np.pi, -2, 2])
    plt.title("2sin(x)", fontsize=8)
    
    plt.subplot(224)
    plt.plot(x, 2*np.sin(2*x), 'm:')
    plt.axis([0, 2*np.pi, -2, 2])
    plt.title("2sin(2x)", fontsize=8)
    
    #Give figure a title and show plots
    plt.suptitle("Variations of sin(x):")
    plt.show()


# Problem 5
def prob5():
    """Visualize the data in FARS.npy. Use np.load() to load the data, then
    create a single figure with two subplots:
        1. A scatter plot of longitudes against latitudes. Because of the
            large number of data points, use black pixel markers (use "k,"
            as the third argument to plt.plot()). Label both axes.
        2. A histogram of the hours of the day, with one bin per hour.
            Label and set the limits of the x-axis.
    """
    data = np.load('FARS.npy')
    
    #Create input values x
    accident_hours = data[:, 0]
    accident_longit = data[:, 1]
    accident_latit = data[:, 2]
    
    #Change size of figure
    plt.figure(figsize=(9, 5))
    
    #Create subplots
    plt.subplot(121)
    plt.plot(accident_longit, accident_latit, 'k,', markersize=0.5)
    plt.xlabel("Longitude")
    plt.ylabel("Latitude")
    plt.axis("equal")
    plt.title("Location")
    
    plt.subplot(122)
    plt.hist(accident_hours, bins=np.arange(0,25))
    plt.xlabel("Hour")
    plt.ylabel("Number of accidents")
    plt.title("Hours")
    
    #Give figure a title and show plots
    plt.suptitle("Car Accident Visualization:")
    plt.show()


# Problem 6
def prob6():
    """Plot the function f(x,y) = sin(x)sin(y)/xy on the domain
    [-2pi, 2pi]x[-2pi, 2pi].
        1. Create 2 subplots: one with a heat map of f, and one with a contour
            map of f. Choose an appropriate number of level curves, or specify
            the curves yourself.
        2. Set the limits of each subplot to [-2pi, 2pi]x[-2pi, 2pi].
        3. Choose a non-default color scheme.
        4. Add a colorbar to each subplot.
    """
    # Create a 2-D domain with np.meshgrid().
    x = np.linspace(-2*np.pi, 2*np.pi, 100)
    y = x.copy()
    X, Y = np.meshgrid(x, y)
    Z = (np.sin(X)*np.sin(Y))/ (X*Y) # Calculate.
    
    # Plot the heat map of f over the 2-D domain.
    plt.subplot(121)
    plt.subplots_adjust(wspace=0.3)
    plt.pcolormesh(X, Y, Z, cmap="viridis")
    plt.colorbar()
    plt.xlim(-2*np.pi, 2*np.pi)
    plt.ylim(-2*np.pi, 2*np.pi)
    plt.title("Heat map of g")
    
    # Plot a contour map of f with 10 level curves.
    plt.subplot(122)
    plt.contour(X, Y, Z, 10, cmap="coolwarm")
    plt.colorbar()
    plt.title("Contour map of g")
    plt.show()

