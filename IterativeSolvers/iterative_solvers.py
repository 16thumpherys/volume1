# iterative_solvers.py
"""Volume 1: Iterative Solvers.
<Tyler Humpherys>
<Math 347>
<3/8/22>
"""

import numpy as np
from scipy import linalg as la
from scipy import sparse
from matplotlib import pyplot as plt


# Helper function
def diag_dom(n, num_entries=None, as_sparse=False):
    """Generate a strictly diagonally dominant (n, n) matrix.
    Parameters:
        n (int): The dimension of the system.
        num_entries (int): The number of nonzero values.
            Defaults to n^(3/2)-n.
        as_sparse: If True, an equivalent sparse CSR matrix is returned.
    Returns:
        A ((n,n) ndarray): A (n, n) strictly diagonally dominant matrix.
    """
    if num_entries is None:
        num_entries = int(n**1.5) - n
    A = sparse.dok_matrix((n,n))
    rows = np.random.choice(n, size=num_entries)
    cols = np.random.choice(n, size=num_entries)
    data = np.random.randint(-4, 4, size=num_entries)
    for i in range(num_entries):
        A[rows[i], cols[i]] = data[i]
    B = A.tocsr()          # convert to row format for the next step
    for i in range(n):
        A[i,i] = abs(B[i]).sum() + 1
    return A.tocsr() if as_sparse else A.toarray()


# Problems 1 and 2
def jacobi(A, b, tol=1e-8, maxiter=100, plot=False):
    """Calculate the solution to the system Ax = b via the Jacobi Method.

    Parameters:
        A ((n,n) ndarray): A square matrix.
        b ((n ,) ndarray): A vector of length n.
        tol (float): The convergence tolerance.
        maxiter (int): The maximum number of iterations to perform.

    Returns:
        ((n,) ndarray): The solution to system Ax = b.
    """
    #Initialize iterations, initial x, and the inverse D matrix
    iterations, x_init, D_inverse = 0, np.zeros(A.shape[0]), np.diag(np.ones(A.shape[0])/np.diag(A))
    
    #Compute first iteration
    x_next = x_init + D_inverse@(b - A@x_init)
    
    #Create lists to keep track of errors
    abs_error = [la.norm(A@x_next - b, ord=np.inf)]
    
    while (iterations < maxiter) and (la.norm(x_init - x_next, ord=np.inf) >= tol):
        #Compute next iteration
        x_init = x_next
        x_next = x_init + D_inverse@(b - A@x_init)
        
        #Compute error and add to list
        abs_error.append(la.norm(A@x_next - b, ord=np.inf))
        
        #Increase iteration by one
        iterations += 1
        
    if plot:
        #Plot error against iterations if plot is true
        plt.semilogy(range(iterations + 1), abs_error)
        plt.xlabel('Iteration Number')
        plt.ylabel('Abs Error')
        plt.title('Jacobi Method')
        plt.show()
        
    return x_next
    

# Problem 3
def gauss_seidel(A, b, tol=1e-8, maxiter=100, plot=False):
    """Calculate the solution to the system Ax = b via the Gauss-Seidel Method.

    Parameters:
        A ((n, n) ndarray): A square matrix.
        b ((n, ) ndarray): A vector of length n.
        tol (float): The convergence tolerance.
        maxiter (int): The maximum number of iterations to perform.
        plot (bool): If true, plot the convergence rate of the algorithm.

    Returns:
        x ((n,) ndarray): The solution to system Ax = b.
    """
    #Initialize iterations and initial x
    iterations, x_init = 0, np.ones(b.shape[0])
    
    #Compute first iteration
    for i in range(b.shape[0]):
        x_next = x_init + (1/A[i,i]) * (b[i] - A.T[i]@x_init)
    
    #Create lists to keep track of errors
    abs_error = [la.norm(A@x_next - b, ord=np.inf)]
    
    while (iterations < maxiter) and (la.norm(x_init - x_next, ord=np.inf) >= tol):
        #Compute next iteration
        x_init = np.copy(x_next)
        for i in range(b.shape[0]):
            x_next = x_init + (1/A[i,i]) * (b[i] - A.T[i]@x_init)
        
        #Compute error and add to list
        abs_error.append(la.norm(A@x_next - b, ord=np.inf))
        
        #Increase iteration by one
        iterations += 1
        
    if plot:
        #Plot error against iterations if plot is true
        plt.semilogy(range(iterations + 1), abs_error)
        plt.xlabel('Iteration Number')
        plt.ylabel('Abs Error')
        plt.title('Gauss-Seidel Method')
        plt.show()
        
    return x_next


# Problem 4
def gauss_seidel_sparse(A, b, tol=1e-8, maxiter=100):
    """Calculate the solution to the sparse system Ax = b via the Gauss-Seidel
    Method.

    Parameters:
        A ((n, n) csr_matrix): A (n, n) sparse CSR matrix.
        b ((n, ) ndarray): A vector of length n.
        tol (float): The convergence tolerance.
        maxiter (int): the maximum number of iterations to perform.

    Returns:
        x ((n,) ndarray): The solution to system Ax = b.
    """
    #Initialize iterations, initial x, and get the diagonal of A
    x_init, diag = np.zeros(b.shape[0]), A.diagonal()
    
    # Go through the iteration at most maxiter times
    for _ in range(maxiter):
        x_next = x_init.copy()
        
        for i in range(b.shape[0]):
            #Get the indices of where the i-th row of A starts and ends if the nonzero entries of A were flattened
            rowstart = A.indptr[i]
            rowend = A.indptr[i+1]
            
            #Multiply only the nonzero elements of the i-th row of A with the corresponding elements of x
            Aix = A.data[rowstart:rowend] @ x_next[A.indices[rowstart:rowend]]
            x_next[i] = x_init[i] + (1/diag[i]) * (b[i] - Aix)
        
        #Break if the difference is small enough
        if la.norm(x_init - x_next) < tol:
            break
        
        #Switch variables to keep iterating
        x_init = x_next
        
    return x_next
    

# Problem 5
def sor(A, b, omega, tol=1e-8, maxiter=100):
    """Calculate the solution to the system Ax = b via Successive Over-
    Relaxation.

    Parameters:
        A ((n, n) csr_matrix): A (n, n) sparse matrix.
        b ((n, ) Numpy Array): A vector of length n.
        omega (float in [0,1]): The relaxation factor.
        tol (float): The convergence tolerance.
        maxiter (int): The maximum number of iterations to perform.

    Returns:
        ((n,) ndarray): The solution to system Ax = b.
        (bool): Whether or not Newton's method converged.
        (int): The number of iterations computed.
    """
    #Initialize iterations, initial x, and get the diagonal of A
    x_init, diag, converged_status = np.zeros(b.shape[0]), A.diagonal(), False
    
    for iteration in range(maxiter):
        #Create copy of x init
        x_next = x_init.copy()
        
        for i in range(b.shape[0]):
            #Get the indices of where the i-th row of A starts and ends if the nonzero entries of A were flattened
            rowstart = A.indptr[i]
            rowend = A.indptr[i+1]
            
            #Multiply only the nonzero elements of the i-th row of A with the corresponding elements of x
            Aix = A.data[rowstart:rowend] @ x_next[A.indices[rowstart:rowend]]
            x_next[i] = x_init[i] + (omega/diag[i]) * (b[i] - Aix)
        
        #Break if the difference is small enough
        if la.norm(x_init - x_next) < tol:
            converged_status = True
            break
        
        #Switch variables to keep iterating
        x_init = x_next
        
    return x_next, converged_status, iteration + 1


# Problem 6
def hot_plate(n, omega, tol=1e-8, maxiter=100, plot=False):
    """Generate the system Au = b and then solve it using sor().
    If show is True, visualize the solution with a heatmap.

    Parameters:
        n (int): Determines the size of A and b.
            A is (n^2, n^2) and b is one-dimensional with n^2 entries.
        omega (float in [0,1]): The relaxation factor.
        tol (float): The iteration tolerance.
        maxiter (int): The maximum number of iterations.
        plot (bool): Whether or not to visualize the solution.

    Returns:
        ((n^2,) ndarray): The 1-D solution vector u of the system Au = b.
        (bool): Whether or not Newton's method converged.
        (int): The number of computed iterations in SOR.
    """
    #Initialize matrix B and A
    B_mat = sparse.diags([1, -4, 1], [-1, 0, 1], shape=(n,n))
    A_mat = sparse.block_diag([B_mat] * n)
    A_mat.setdiag(1, -n)
    A_mat.setdiag(1, n)

    #Use tile to create the solution vector b
    b_vec = np.tile(np.concatenate(([-100], [0] * (n - 2), [-100])), n)

    #Call problem 5
    X, converged_status, iterations = sor(A_mat.tocsr(), b_vec, omega=omega, tol=tol, maxiter=maxiter)

    if plot:
        #Plot heatmap
        plt.pcolormesh(X.reshape((n,n)), cmap="coolwarm")
        plt.title('Heat distribution on hot plate')
        plt.show()

    return X, converged_status, iterations


# Problem 7
def prob7():
    """Run hot_plate() with omega = 1, 1.05, 1.1, ..., 1.9, 1.95, tol=1e-2,
    and maxiter = 1000 with A and b generated with n=20. Plot the iterations
    computed as a function of omega.
    """
    #Initialize list to store number of iterations and the omega values
    omega_values = np.linspace(1, 2, 20)
    
    #Compute the number of iterations it takes with 
    iterations = [hot_plate(20, omega=omega_val, tol=1e-2, maxiter=1000, plot=False)[2] for omega_val in omega_values]
    
    #Plot
    plt.plot(omega_values, iterations)
    plt.xlabel('Omega Value')
    plt.ylabel('Iterations to compute')
    plt.title('Iterations needed for different omega values')
    plt.show()

    return omega_values[np.argmin(iterations)]
    
