# lstsq_eigs.py
"""Volume 1: Least Squares and Computing Eigenvalues.
<Tyler Humpherys>
<Math 345>
<10/26/21>
"""
import numpy as np
from scipy import linalg as la
from matplotlib import pyplot as plt
import cmath

# Problem 1
def least_squares(A, b):
    """Calculate the least squares solutions to Ax = b by using the QR
    decomposition.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n <= m.
        b ((m, ) ndarray): A vector of length m.

    Returns:
        x ((n, ) ndarray): The solution to the normal equations.
    """
    #Find Q and R
    Q, R = la.qr(A, mode="economic")
    #Solve the system using Scipy's solver
    return la.solve_triangular(R, Q.T@b)


# Problem 2
def line_fit():
    """Find the least squares line that relates the year to the housing price
    index for the data in housing.npy. Plot both the data points and the least
    squares line.
    """
    #Create A and b to use to find the least squares solution
    years, b = np.load('housing.npy')[:, 0], np.load('housing.npy')[:, 1]
    ones = np.ones(np.shape(years))
    A = np.column_stack((years, ones))
    
    #Find the least squares solution
    x = least_squares(A, b)
    
    #Get the line of best fit
    y = [x[0]*year + x[1] for year in years]
    
    #Plot the original data and least squares solution
    plt.plot(years, b, 'b.', label="Data Points")
    plt.plot(years, y, 'r-', label="Least Squares Fit")
    plt.xlabel("Year (in 2000's)")
    plt.ylabel("Index")
    plt.legend(loc='upper left')
    plt.title("Least Squares for Housing Prices")
    plt.show()


# Problem 3
def polynomial_fit():
    """Find the least squares polynomials of degree 3, 6, 9, and 12 that relate
    the year to the housing price index for the data in housing.npy. Plot both
    the data points and the least squares polynomials in individual subplots.
    """
    #Load housing data
    years, b = np.load('housing.npy')[:, 0], np.load('housing.npy')[:, 1]
    
    #Create the matrix A for each polynomial of distinct powers
    A_3, A_6, A_9, A_12 = np.vander(years, 4), np.vander(years, 7), np.vander(years, 10), np.vander(years, 13)
    
    #Find the least squaes solutions for each polynomial of distinct powers
    x_3, x_6, x_9, x_12 = la.lstsq(A_3, b)[0], la.lstsq(A_6, b)[0], la.lstsq(A_9, b)[0], la.lstsq(A_12, b)[0]
    
    #Get the line of best fit for each polynomial of distinct powers
    y_3, y_6 = [A_3[i]@x_3 for i in range(len(A_3))], [A_6[i]@x_6 for i in range(len(A_6))]
    y_9, y_12 = [A_9[i]@x_9 for i in range(len(A_9))], [A_12[i]@x_12 for i in range(len(A_12))]
    
    #Plot the original data and least squares solution
    plt.subplot(221)
    plt.plot(years, b, 'b.', label="Data Points")
    plt.plot(years, y_3, 'r-', label="Least Squares Fit")
    plt.xlabel("Year (in 2000's)")
    plt.ylabel("Index")
    plt.legend(loc='lower right')
    plt.title("Degree 3")
    
    plt.subplot(222)
    plt.plot(years, b, 'b.', label="Data Points")
    plt.plot(years, y_6, 'r-', label="Least Squares Fit")
    plt.xlabel("Year (in 2000's)")
    plt.ylabel("Index")
    plt.legend(loc='lower right')
    plt.title("Degree 6")
    
    plt.subplot(223)
    plt.plot(years, b, 'b.', label="Data Points")
    plt.plot(years, y_9, 'r-', label="Least Squares Fit")
    plt.xlabel("Year (in 2000's)")
    plt.ylabel("Index")
    plt.legend(loc='lower right')
    plt.title("Degree 9")
    
    plt.subplot(224)
    plt.plot(years, b, 'b.', label="Data Points")
    plt.plot(years, y_12, 'r-', label="Least Squares Fit")
    plt.xlabel("Year (in 2000's)")
    plt.ylabel("Index")
    plt.legend(loc='lower right')
    plt.title("Degree 12")
    
    plt.suptitle("Least Squares for Housing Prices")
    plt.tight_layout()
    plt.show()
    
def plot_ellipse(a, b, c, d, e):
    """Plot an ellipse of the form ax^2 + bx + cxy + dy + ey^2 = 1."""
    theta = np.linspace(0, 2*np.pi, 200)
    cos_t, sin_t = np.cos(theta), np.sin(theta)
    A = a*(cos_t**2) + c*cos_t*sin_t + e*(sin_t**2)
    B = b*cos_t + d*sin_t
    r = (-B + np.sqrt(B**2 + 4*A)) / (2*A)

    plt.plot(r*cos_t, r*sin_t)
    plt.gca().set_aspect("equal", "datalim")

# Problem 4
def ellipse_fit():
    """Calculate the parameters for the ellipse that best fits the data in
    ellipse.npy. Plot the original data points and the ellipse together, using
    plot_ellipse() to plot the ellipse.
    """
    #Load ellipse data and construct the matrix A and the vector b.
    xk, yk = np.load("ellipse.npy").T
    
    A = np.column_stack((xk**2, xk, xk*yk, yk, yk**2))
    b = np.ones(np.shape(xk)[0])
    
    #Calculate the least squares solution
    c1, c2, c3, c4, c5 = la.lstsq(A, b)[0]
    
    #Plot the ellipse
    plot_ellipse(c1, c2, c3, c4, c5)
    
    #Plot the data points.
    plt.plot(xk, yk, 'k*')
    plt.title("Least Squares fit to Ellipse")
    plt.axis("equal")
    plt.show()


# Problem 5
def power_method(A, N=20, tol=1e-12):
    """Compute the dominant eigenvalue of A and a corresponding eigenvector
    via the power method.

    Parameters:
        A ((n,n) ndarray): A square matrix.
        N (int): The maximum number of iterations.
        tol (float): The stopping tolerance.

    Returns:
        (float): The dominant eigenvalue of A.
        ((n,) ndarray): An eigenvector corresponding to the dominant
            eigenvalue of A.
    """
    #Get the diensions of A
    m, n = np.shape(A)
    
    #Create a random vector of length n
    x_0 = np.random.random(n)
    
    #Normalize x_0
    x_0 = x_0/la.norm(x_0)
    
    #Begin iterations
    for k in range(N):
        x_k = A@x_0
        x_k = x_k/la.norm(x_k)
        
        #Break if the distance becomes less than the tolerance
        if la.norm(x_k - x_0) < tol:
            break
        
        #Otherwise keep iterating
        else:
            x_0 = x_k
    return x_k.T@A@x_k, x_k


# Problem 6
def qr_algorithm(A, N=50, tol=1e-12):
    """Compute the eigenvalues of A via the QR algorithm.

    Parameters:
        A ((n,n) ndarray): A square matrix.
        N (int): The number of iterations to run the QR algorithm.
        tol (float): The threshold value for determining if a diagonal S_i
            block is 1x1 or 2x2.

    Returns:
        ((n,) ndarray): The eigenvalues of A.
    """
    #Get the dimensions of A
    m, n = np.shape(A)
    
    #Put A in upper Hessenberg form
    S = la.hessenberg(A)
    
    for k in range(N):
        #Get the QR decomposition of Ak
        Q, R = la.qr(S)
        
        #Recombine Rk and Qk into Ak+1
        S = R@Q
    
    #Create an empty list to store my eigenvalues
    eigs = []
    
    #Intitialize the index to 0
    i = 0
    
    while i < n:
        #If the shape of the vector is 1 X 1 then append it to the eigenvalues
        if (i == n-1) or (np.abs(S[i+1][i]) < tol):
            eigs.append(S[i][i])
            
        
        #Otherwise
        else:
            #Calculate the eigenvalues of S[i]
            a, b, c, d = S[i][i], S[i][i+1], S[i+1][i], S[i+1][i+1]
            eigenvalue1 = ((a+d) + cmath.sqrt(((-1*d - a)**2) - (4*(a*d - b*c)))) / 2
            eigenvalue2 = ((a+d) - cmath.sqrt(((-1*d - a)**2) - (4*(a*d - b*c)))) / 2
            
            #Append eigenvalues to eigs
            eigs.append(eigenvalue1)
            eigs.append(eigenvalue2)
            
            i += 1
        
        #Move to the next S[i]
        i += 1
    
    return np.array(eigs)

