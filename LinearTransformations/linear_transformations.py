# linear_transformations.py
"""Volume 1: Linear Transformations.
<Tyler Humpherys>
<Math 345>
<9/21/21>
"""

from random import random
import numpy as np
from matplotlib import pyplot as plt
import time


# Problem 1
def stretch(A, a, b):
    """Scale the points in A by a in the x direction and b in the
    y direction.

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): scaling factor in the x direction.
        b (float): scaling factor in the y direction.
    """
    #Create the matrix
    stretchMatrix = np.array([[a, 0],[0, b]])
    #Multiply the two matrices together
    return np.matmul(stretchMatrix, A)

def shear(A, a, b):
    """Slant the points in A by a in the x direction and b in the
    y direction.

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): scaling factor in the x direction.
        b (float): scaling factor in the y direction.
    """
    #Create the Matrix
    stretchMatrix = np.array([[1, a],[b, 1]])
    #Multiply the two matrices together
    return np.matmul(stretchMatrix, A)

def reflect(A, a, b):
    """Reflect the points in A about the line that passes through the origin
    and the point (a,b).

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): x-coordinate of a point on the reflecting line.
        b (float): y-coordinate of the same point on the reflecting line.
    """
    #Create matrix
    stretchMatrix = (1/(a**2 + b**2)) * np.array([[(a**2 - b**2), 2*a*b],[2*a*b, (b**2 - a**2)]])
    #Multiply the two matrices together
    return np.matmul(stretchMatrix, A)

def rotate(A, theta):
    """Rotate the points in A about the origin by theta radians.

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        theta (float): The rotation angle in radians.
    """
    #Create matrix
    stretchMatrix = np.array([[np.cos(theta), -np.sin(theta)],[np.sin(theta), np.cos(theta)]])
    #Multiply the two matrices together
    return np.matmul(stretchMatrix, A)

#Load the horse npy file
horseData = np.load("horse.npy")

#Function to plot the original points with the transformed points
def display(data):
    #Apply transformations
    stretchData = stretch(horseData, 0.5, 1)
    shearData = shear(horseData, 0.5, 0)
    reflectData = reflect(horseData, 0, 1)
    rotateData = rotate(horseData, np.pi/2)
    compositeData = reflect(rotate(shear(horseData, 0, 0.5), np.pi/2), 1, 0)
    
    #Put all data in a list
    transformed = [horseData, stretchData, shearData, reflectData, rotateData, compositeData]
    
    #Create subplots
    fig, ax = plt.subplots(nrows=2, ncols=3, figsize=(15,15))
    
    #Plot all data
    for i, row in enumerate(ax):
        for j, col in enumerate(row):
            pos = (3 * i) + j
            col.plot(transformed[pos][0], transformed[pos][1], 'k,')
            col.axis([-1,1,-1,1])
            
    plt.gca().set_aspect("equal")
    plt.show()

#print(display(horseData))

# Problem 2
def solar_system(T, x_e, x_m, omega_e, omega_m):
    """Plot the trajectories of the earth and moon over the time interval [0,T]
    assuming the initial position of the earth is (x_e,0) and the initial
    position of the moon is (x_m,0).

    Parameters:
        T (int): The final time.
        x_e (float): The earth's initial x coordinate.
        x_m (float): The moon's initial x coordinate.
        omega_e (float): The earth's angular velocity.
        omega_m (float): The moon's angular velocity.
    """
    #Rotate the initial vector counterclockwise
    #Create time values
    time = np.linspace(0, T, 1000)
    pe_0 = np.array([x_e, 0])
    pm_0 = np.array([x_m, 0])
    
    #Compute earth's position
    pe_t = np.array([rotate(pe_0, (t*omega_e)) for t in time]).T
    
    #Calculate the moon's position relataive to the earth at time t
    pm_t = np.array([rotate((pm_0 - pe_0), t * omega_m) + rotate(pe_0, (t*omega_e)) for t in time]).T
    
    #Plot earth position
    plt.plot(pe_t[0], pe_t[1], 'tab:blue', label="Earth", lw=3)
    #Plot moon position
    plt.plot(pm_t[0], pm_t[1], 'tab:orange', label="Moon", lw=3)
    
    plt.gca().set_aspect("equal")
    plt.legend(loc = "lower right")
    plt.show()
    

def random_vector(n):
    """Generate a random vector of length n as a list."""
    return [random() for i in range(n)]

def random_matrix(n):
    """Generate a random nxn matrix as a list of lists."""
    return [[random() for j in range(n)] for i in range(n)]

def matrix_vector_product(A, x):
    """Compute the matrix-vector product Ax as a list."""
    m, n = len(A), len(x)
    return [sum([A[i][k] * x[k] for k in range(n)]) for i in range(m)]

def matrix_matrix_product(A, B):
    """Compute the matrix-matrix product AB as a list of lists."""
    m, n, p = len(A), len(B), len(B[0])
    return [[sum([A[i][k] * B[k][j] for k in range(n)])
                                    for j in range(p) ]
                                    for i in range(m) ]

# Problem 3
def prob3():
    """Use time.time(), timeit.timeit(), or %timeit to time
    matrix_vector_product() and matrix-matrix-mult() with increasingly large
    inputs. Generate the inputs A, x, and B with random_matrix() and
    random_vector() (so each input will be nxn or nx1).
    Only time the multiplication functions, not the generating functions.

    Report your findings in a single figure with two subplots: one with matrix-
    vector times, and one with matrix-matrix times. Choose a domain for n so
    that your figure accurately describes the growth, but avoid values of n
    that lead to execution times of more than 1 minute.
    """
    #Create n values to plot
    n_times = [2**n for n in range(1,9)]
    mat_vec_vals = []
    mat_mat_vals = []
    
    for n in n_times:
        #Generate random matrix and vector of size n
        random_mat = random_matrix(n)
        random_vec = random_vector(n)
        
        #Time matrix-vector computation
        start = time.time()
        matrix_vector_product(random_mat, random_vec)
        comp_time = time.time() - start
        mat_vec_vals.append(comp_time)
        
        #Time matrix-matrix computation
        start = time.time()
        matrix_matrix_product(random_mat, random_mat)
        comp_time = time.time() - start
        mat_mat_vals.append(comp_time)
        
    
    #Plot matrix-vector computation times
    plt.subplot(121)
    plt.plot(n_times, mat_vec_vals, '.-', color='tab:blue')
    plt.axis([0, 260, 0, 0.007])
    plt.xlabel("n")
    plt.ylabel("Seconds")
    plt.title("Matrix-Vector Multiplication")
    
    #Plot matrix-matrix computation times
    plt.subplot(122)
    plt.plot(n_times, mat_mat_vals, '.-', color='tab:orange')
    plt.axis([0, 260, 0, 2.5])
    plt.title("Matrix-Matrix Multiplication")
    plt.xlabel("n")
    plt.show()

# Problem 4
def prob4():
    """Time matrix_vector_product(), matrix_matrix_product(), and np.dot().

    Report your findings in a single figure with two subplots: one with all
    four sets of execution times on a regular linear scale, and one with all
    four sets of exections times on a log-log scale.
    """
    #Create n values to plot
    n_times = [2**n for n in range(1,9)]
    mat_vec_vals = []
    mat_mat_vals = []
    numpy_mat_vec_vals = []
    numpy_mat_mat_vals = []
    
    for n in n_times:
        #Generate random matrix and vector of size n
        random_mat = random_matrix(n)
        random_vec = random_vector(n)
        
        #Time matrix-vector computation
        start = time.time()
        matrix_vector_product(random_mat, random_vec)
        comp_time = time.time() - start
        mat_vec_vals.append(comp_time)
        
        #Time matrix-matrix computation
        start = time.time()
        matrix_matrix_product(random_mat, random_mat)
        comp_time = time.time() - start
        mat_mat_vals.append(comp_time)
        
        #Time numpy matrix-vector computation
        start = time.time()
        np.array(random_mat)@np.array(random_vec)
        comp_time = time.time() - start
        numpy_mat_vec_vals.append(comp_time)
        
        #Time numpy matrix-matrix computation
        start = time.time()
        np.array(random_mat)@np.array(random_mat)
        comp_time = time.time() - start
        numpy_mat_mat_vals.append(comp_time)
        
    
    #Plot computation times on a linear scale
    plt.subplot(121)
    plt.plot(n_times, mat_vec_vals, '.-', color='tab:blue', label="Matrix-Vector with lists")
    plt.plot(n_times, mat_mat_vals, '.-', color='tab:orange', label="Matrix-Matrix with lists")
    plt.plot(n_times, numpy_mat_vec_vals, '.-', color='tab:red', label="Matrix-Vector with Numpy")
    plt.plot(n_times, numpy_mat_mat_vals, '.-', color='tab:green', label="Matrix-Matrix with Numpy")
    plt.xlabel("n")
    plt.legend(loc='upper left')
    plt.ylabel("Seconds")
    plt.title("Linear scale")
    
    #Plot computation times on a log-log scale
    plt.subplot(122)
    plt.loglog(n_times, mat_vec_vals, '.-', color='tab:blue', label="Matrix-Vector with lists")
    plt.loglog(n_times, mat_mat_vals, '.-', color='tab:orange', label="Matrix-Matrix with lists")
    plt.loglog(n_times, numpy_mat_vec_vals, '.-', color='tab:red', label="Matrix-Vector with Numpy")
    plt.loglog(n_times, numpy_mat_mat_vals, '.-', color='tab:green', label="Matrix-Matrix with Numpy")
    plt.title("Log-log scale")
    plt.legend(loc='upper left')
    plt.xlabel("n")
    plt.show()

