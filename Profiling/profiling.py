# profiling.py
"""Python Essentials: Profiling.
<Tyler Humpherys>
<Math 347>
<1/4/22>
""" 

# Note: for problems 1-4, you need only implement the second function listed.
# For example, you need to write max_path_fast(), but keep max_path() unchanged
# so you can do a before-and-after comparison.

import numpy as np
import time
from numba import jit
from matplotlib import pyplot as plt



# Problem 1
def max_path(filename="triangle.txt"):
    """Find the maximum vertical path in a triangle of values."""
    with open(filename, 'r') as infile:
        data = [[int(n) for n in line.split()]
                        for line in infile.readlines()]
    def path_sum(r, c, total):
        """Recursively compute the max sum of the path starting in row r
        and column c, given the current total.
        """
        total += data[r][c]
        if r == len(data) - 1:          # Base case.
            return total
        else:                           # Recursive case.
            return max(path_sum(r+1, c,   total),   # Next row, same column
                       path_sum(r+1, c+1, total))   # Next row, next column

    return path_sum(0, 0, 0)            # Start the recursion from the top.

def max_path_fast(filename="triangle_large.txt"):
    """Find the maximum vertical path in a triangle of values."""
    #Open file and tailor data
    with open(filename, 'r') as infile:
        data = [[int(n) for n in line.split()]
                        for line in infile.readlines()]
    numRows = len(data)
    
    #Compute sum of path
    if (numRows > 1):
        data[1][1] = data[1][1] + data[0][0]
        data[1][0] = data[1][0] + data[0][0]
    for row in range(2, numRows):
        data[row][0] = data[row][0] + data[row - 1][0]
        data[row][row] = data[row][row] + data[row - 1][row - 1]
        for col in range(1, row):
            if (data[row][col] + data[row - 1][col - 1]) >= (data[row][col] + data[row - 1][col]):
                data[row][col] = data[row][col] + data[row - 1][col - 1]
            else:
                data[row][col] = data[row][col] + data[row - 1][col]   
    pathSums = data[numRows-1]
    
    #Remove repitive sums
    newPathSums = []
    [newPathSums.append(aSum) for aSum in pathSums if aSum not in newPathSums]
    
    #Sort in increasing order
    newPathSums.sort()
    
    #Return the largest path sum
    return newPathSums[-1]

# Problem 2
def primes(N):
    """Compute the first N primes."""
    primes_list = []
    current = 2
    while len(primes_list) < N:
        isprime = True
        for i in range(2, current):     # Check for nontrivial divisors.
            if current % i == 0:
                isprime = False
        if isprime:
            primes_list.append(current)
        current += 1
    return primes_list

def primes_fast(N):
    """Compute the first N primes."""
    #Intitialize list and current value
    primes_list = [2]
    current = 3
    num_primes = 1
    
    while num_primes < N:
        isprime = True
        value = int(np.sqrt(current))
        
        for i in primes_list:     # Check for nontrivial divisors.
            #If there is a nontrivial divisor then mark as not prime
            if i > value:
                break
            
            if current % i == 0:
                isprime = False
                break
                
        #If there are no nontrivial divisors then add it to prime list
        if isprime:
            primes_list.append(current)
            num_primes += 1
            
        #Only check odd numbers
        current += 2
        
    return primes_list

# Problem 3
def nearest_column(A, x):
    """Find the index of the column of A that is closest to x.

    Parameters:
        A ((m,n) ndarray)
        x ((m, ) ndarray)

    Returns:
        (int): The index of the column of A that is closest in norm to x.
    """
    distances = []
    for j in range(A.shape[1]):
        distances.append(np.linalg.norm(A[:,j] - x))
    return np.argmin(distances)

def nearest_column_fast(A, x):
    """Find the index of the column of A that is closest in norm to x.
    Refrain from using any loops or list comprehensions.

    Parameters:
        A ((m,n) ndarray)
        x ((m, ) ndarray)

    Returns:
        (int): The index of the column of A that is closest in norm to x.
    """
    return np.argmin(np.linalg.norm(A - x[:, np.newaxis], axis=0))


# Problem 4
def name_scores(filename="names.txt"):
    """Find the total of the name scores in the given file."""
    with open(filename, 'r') as infile:
        names = sorted(infile.read().replace('"', '').split(','))
    total = 0
    for i in range(len(names)):
        name_value = 0
        for j in range(len(names[i])):
            alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            for k in range(len(alphabet)):
                if names[i][j] == alphabet[k]:
                    letter_value = k + 1
            name_value += letter_value
        total += (names.index(names[i]) + 1) * name_value
    return total

def name_scores_fast(filename='names.txt'):
    """Find the total of the name scores in the given file."""
    #Read in the file of names and sort them alphabetically
    with open(filename, 'r') as infile:
        names = sorted(infile.read().replace('"', '').split(','))
    
    #Initialize values and dictionary for efficient searching
    total = 0
    name_index = 1
    numbers = [i for i in range(1, 27)]
    alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    dictionary = dict(zip(alphabet, numbers))
    
    #Compute the name score of each name in the list
    for name in names:
        name_value = 0
        for letter in name:
            letter_value = dictionary[letter]
            name_value += letter_value
        total += name_index * name_value
        name_index += 1
    return total


# Problem 5
def fibonacci():
    """Yield the terms of the Fibonacci sequence with F_1 = F_2 = 1."""
    #Initialize values
    F_prev1, F_prev2 = 1, 1
    
    while True:
        
        #Compute next fibonacci number
        yield F_prev1
        F_prev1 = F_prev1 + F_prev2
        
        yield F_prev2
        F_prev2 = F_prev1 + F_prev2


def fibonacci_digits(N=1000):
    """Return the index of the first term in the Fibonacci sequence with
    N digits.

    Returns:
        (int): The index.
    """
    for i, n in enumerate(fibonacci()):
        
        #Check the number of digits
        if len(str(n)) == N:
            return i + 1

# Problem 6
def prime_sieve(N):
    """Yield all primes that are less than N."""
    #Initialize numbers
    numbers = np.arange(2, N+1)
    
    while True:
        #Check the first number against all others
        first_num = numbers[0]
        
        #If the first number divides another in the list remove that number
        mask = tuple([numbers % first_num != 0])
        numbers = numbers[mask]
        
        #Yield the first num and then break if the list is empty
        yield first_num
        if len(numbers) == 0:
            break
        

# Problem 7
def matrix_power(A, n):
    """Compute A^n, the n-th power of the matrix A."""
    product = A.copy()
    temporary_array = np.empty_like(A[0])
    m = A.shape[0]
    for power in range(1, n):
        for i in range(m):
            for j in range(m):
                total = 0
                for k in range(m):
                    total += product[i,k] * A[k,j]
                temporary_array[j] = total
            product[i] = temporary_array
    return product

@jit
def matrix_power_numba(A, n):
    """Compute A^n, the n-th power of the matrix A, with Numba optimization."""
    product = A.copy()
    temporary_array = np.empty_like(A[0])
    m = A.shape[0]
    for power in range(1, n):
        for i in range(m):
            for j in range(m):
                total = 0
                for k in range(m):
                    total += product[i,k] * A[k,j]
                temporary_array[j] = total
            product[i] = temporary_array
    return product

def prob7(n=10):
    """Time matrix_power(), matrix_power_numba(), and np.linalg.matrix_power()
    on square matrices of increasing size. Plot the times versus the size.
    """
    #Initialize lists
    m_vals = [2**i for i in range(2,8)]
    regular_times, numba_times, numpy_times = [], [], []
    
    #Test call to speed up process later
    call = matrix_power_numba(np.array([[1,1],[2,3]]), 2)
    
    for m in m_vals:
        #Initialize random mxm matrix
        A = np.random.random((m,m))
        
        #Time matrix_power()
        start = time.time()
        comp1 = matrix_power(A, n)
        end1 = time.time() - start
        regular_times.append(end1)
        
        #Time matrix matrix_power_numba()
        start = time.time()
        comp2 = matrix_power_numba(A, n)
        end2 = time.time() - start
        numba_times.append(end2)
        
        #Time np.linalg.matrix_power()
        start = time.time()
        comp3 = np.linalg.matrix_power(A, n)
        end3 = time.time() - start
        numpy_times.append(end3)
        
    #Plot times
    plt.loglog(m_vals, regular_times, c='r', label='Regular')
    plt.loglog(m_vals, numba_times, c='b', label='Numba')
    plt.loglog(m_vals, numpy_times, c='black', label='Numpy')
    plt.legend(loc='upper left')
    plt.xlabel("n-th power")
    plt.ylabel("Time (s)")
    plt.title("Comparison of Matrix Power Methods")
    plt.tight_layout()
    plt.show()

        
