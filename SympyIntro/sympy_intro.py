# sympy_intro.py
"""Python Essentials: Introduction to SymPy.
<Tyler Humpherys>
<Math 347>
<1/11/22>
"""
import sympy as sy
import numpy as np
from matplotlib import pyplot as plt

# Problem 1
def prob1():
    """Return an expression for

        (2/5)e^(x^2 - y)cosh(x+y) + (3/7)log(xy + 1).

    Make sure that the fractions remain symbolic.
    """
    #Define variables
    x, y = sy.symbols('x, y')
    
    #Write expression
    expression = (sy.Rational(2, 5) * sy.exp((x**2 - y)) * sy.cosh(x+y)) + sy.Rational(3, 7) * sy.log((x*y)+1)
    return expression


# Problem 2
def prob2():
    """Compute and simplify the following expression.

        product_(i=1 to 5)[ sum_(j=i to 5)[j(sin(x) + cos(x))] ]
    """
    #Define variables
    x, j, i = sy.symbols('x, j, i')
    
    #Write expression
    expr = sy.product(sy.summation(j*(sy.sin((x)) + sy.cos(x)), (j, i, 5)), (i, 1, 5))
    return sy.trigsimp(expr)
    

# Problem 3
def prob3(N):
    """Define an expression for the Maclaurin series of e^x up to order N.
    Substitute in -y^2 for x to get a truncated Maclaurin series of e^(-y^2).
    Lambdify the resulting expression and plot the series on the domain
    y in [-3,3]. Plot e^(-y^2) over the same domain for comparison.
    """

    #Define symbols
    x, y, n = sy.symbols('x, y, n')
    
    #Create expression and function
    expr = sy.summation(((x**n)/sy.factorial(n)), (n, 0, N))
    new_expr = expr.subs(x, (-y**2))
    function = sy.lambdify(y, new_expr)
    
    #Plot
    ls = np.linspace(-2, 2, 100)
    plt.plot(ls, [function(t) for t in ls], label=f"Maclaurin series for N={N}", alpha=0.3) 
    
    #Plot exact function
    x = np.linspace(-5, 5, 100)
    y = np.exp(-(x**2))
    plt.plot(x, y, label='f(y)=e^(-y^2)', alpha=0.3)
    plt.xlim(-2, 2)
    plt.ylim(0,1)
    plt.xlabel("y values")
    plt.ylabel("f(y) values")
    plt.legend(loc='lower center')
    plt.title("Maclaurin series")
    plt.show()


# Problem 4
def prob4():
    """The following equation represents a rose curve in cartesian coordinates.

    0 = 1 - [(x^2 + y^2)^(7/2) + 18x^5 y - 60x^3 y^3 + 18x y^5] / (x^2 + y^2)^3

    Construct an expression for the nonzero side of the equation and convert
    it to polar coordinates. Simplify the result, then solve it for r.
    Lambdify a solution and use it to plot x against y for theta in [0, 2pi].
    """
    #Define symbols
    x, y, r, t = sy.symbols('x, y, r, t')
        
    #Create expression and simplify it
    expr = 1 - ((x**2 + y**2)**sy.Rational(7/2) + 18 * (x)**5 * y - 60* (x)**3 * y**3 + 18*(x) *(y)**5) / (x**2 + y**2)**(3)
    trig_expr = sy.trigsimp(expr.subs({x: r*sy.cos(t), y: r*sy.sin(t)}))
    
    #Find a solution to the expression
    solution = sy.lambdify(t, sy.solve(trig_expr)[0].get(r))
    
    #Plot
    lin = np.linspace(0, 2*np.pi, 1000)
    plt.plot(solution(lin)*np.cos(lin), solution(lin)*np.sin(lin))
    plt.xlabel("x(theta)")
    plt.ylabel("y(theta)")
    plt.title("Rose Curve")
    plt.show()
    
    
# Problem 5
def prob5():
    """Calculate the eigenvalues and eigenvectors of the following matrix.

            [x-y,   x,   0]
        A = [  x, x-y,   x]
            [  0,   x, x-y]

    Returns:
        (dict): a dictionary mapping eigenvalues (as expressions) to the
            corresponding eigenvectors (as SymPy matrices).
    """
    #Initiate symbols and matrix A
    x, y, lam = sy.symbols('x, y, lam')
    A = sy.Matrix([[x-y-lam, x, 0], [x, x-y-lam, x], [0, x, x-y-lam]])
    
    #Compute eigenvalues and eigenvectors
    eigenvalues, eigenvectors = sy.solve(sy.det(A), lam), []
    for i in range(3):
        eigenvectors.append(A.subs(lam, eigenvalues[i]).nullspace()[0])
    
    #Map eigenvalues to eigenvectors
    mapping = dict(zip(eigenvalues, eigenvectors))
    
    return mapping


# Problem 6
def prob6():
    """Consider the following polynomial.

        p(x) = 2*x^6 - 51*x^4 + 48*x^3 + 312*x^2 - 576*x - 100

    Plot the polynomial and its critical points. Determine which points are
    maxima and which are minima.

    Returns:
        (set): the local minima.
        (set): the local maxima.
    """
    #Initiate symbols and compute derivatives
    x = sy.symbols('x')
    p = 2*x**6 - 51*x**4 + 48*x**3 + 312*x**2 - 576*x - 100
    fp = sy.lambdify(x, p)
    df = sy.diff(p, x)
    ddf = sy.lambdify(x, sy.diff(df, x))
    
    #Intiate sets for Minima and Maxima. Find the critical points.
    Minima, Maxima, criticalPoints = set(), set(), sy.solve(df, x)

    #Appoint each critical point as a maxima or minima
    for value in criticalPoints:
        if ddf(value) < 0:
            Maxima.add(value)
        else:
            Minima.add(value)

    #Plot line
    ls = np.linspace(-5, 5, 100)
    plt.plot(ls, [fp(t) for t in ls])
    
    #Plot points
    for x in Minima:
        plt.plot(x, fp(x), 'r', marker='o')
    for x in Maxima:
        plt.plot(x, fp(x), 'm', marker='o')
    plt.xlabel("x values")
    plt.ylabel("p(x) values")
    plt.title(f"{p} with critical points")
    plt.show()
    
    return Minima, Maxima


# Problem 7
def prob7():
    """Calculate the integral of f(x,y,z) = (x^2 + y^2 + z^2)^2 over the
    sphere of radius r. Lambdify the resulting expression and plot the integral
    value for r in [0,3]. Return the value of the integral when r = 2.

    Returns:
        (float): the integral of f over the sphere of radius 2.
    """
    #Initiate symbols and expression
    x, y, z, rho, phi, theta, r = sy.symbols('x, y, z, rho, phi, theta, r')
    expr = (x**2 + y**2 + z**2)**2
    
    #Create the matrix h nad the jacobian
    matrix = sy.Matrix([rho*sy.sin(phi)*sy.cos(theta), rho*sy.sin(phi)*sy.sin(theta), rho*sy.cos(phi)])
    jacobian = matrix.jacobian([rho, phi, theta])
    
    #Make subsitutions and compute the integral
    f_new = expr.subs({x: rho*sy.sin(phi)*sy.cos(theta), y: rho*sy.sin(phi)*sy.sin(theta), z: rho*sy.cos(phi)})
    integral = sy.integrate(sy.simplify(f_new * sy.det(jacobian)), (rho, 0, r), (theta, 0, 2*sy.pi), (phi, 0, sy.pi))
    
    #Get the integral expression
    integral_expr = sy.lambdify(r, integral, 'numpy')
    
    #Plot the volume as a function of the radius of the sphere
    lin = np.linspace(0, 3, 1000)
    plt.plot(lin, integral_expr(lin))
    plt.xlabel("radius values")
    plt.ylabel("Integral value/Volume")
    plt.title("Volume of a shere with radius r")
    plt.show()
    
    return integral_expr(2)
   


